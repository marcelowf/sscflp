#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <ilcplex/ilocplex.h>
#include "util.h"
#include "bprice.h"

namespace csp{


typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

void milp(std::string file, const int n_ini, const int n_fim, const int type);
void cg(std::string file, const int n_ini, const int n_fim);


struct Instance{
IloInt wells;
IloInt compressors;
IloInt k; //Points in pressure curve of compressor

//Facilities data
IloNumArray cJ; //Cost facilities
IloNumArray dJ ; //Cost for loss energy
IloNumArray qCMin ; //Min Gas-lift rate 
IloNumArray qCMax ; //Max Gas-lift rate 
IloNumArray alpha0 ;
IloNumArray alpha1 ;
IloNumArray alpha2 ;
IloNumArray alpha3 ;
IloNumArray alpha4 ;
NumNumMatrix pointsQ; //Points with gas-rate of compressors 
NumNumMatrix pointsP; //Points pressure
NumNumMatrix pointsH; //Points with operating costs

//Clients data
IloNumArray qW; //gas-lift rate
IloNumArray pW; //pressure

//Client x Facilities data
NumNumMatrix orac; //Oracle (for subsets)
NumNumMatrix cIJ; //Service cost 
NumNumMatrix lIJ;  //Pressure loss in pipeline
NumNumMatrix qMaxIJ; //Max output rate of compressor for servicing wells

Instance(IloEnv& env){};
void readFile(IloEnv& env, std::ifstream& input);
void completeInstance(IloEnv& env, std::fstream& input, int numPoints);
void completeInstanceEC(IloEnv& env, std::ifstream& inComp, std::ifstream& inWell, std::ifstream& inPipe, int numPoints, std::ofstream& output);
void makeInstance(IloEnv& env, std::ifstream& input, int numPoints, std::ofstream& output);
void makeInstanceHolmberg(IloEnv& env, std::ifstream& input, std::ifstream& ref, int numPoints, std::ofstream& output);
void makeInstanceDelmaire(IloEnv& env, std::ifstream& input, std::ifstream& ref, int numPoints, std::ofstream& output);
};

struct pricing{
 IloObjective SPj;
 IloModel model;
 IloCplex cplex;

 //Variables
 IloNumVarArray x;
 
 //Params - duals
 IloNum mi;
 IloNumArray pi;
 IloArray<IloRangeArray> BranchConst; //B&P
 
 pricing(IloEnv& env) : SPj(env), model(env), cplex(model){
	model.add(SPj);
 };
 
 void init(IloEnv& env, const Instance& inst, const IloInt& idCompressor); //Create the model according to id off compressor j
 //~ void setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const Instance& inst);
 void setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const IloNumArray& psyD, const Instance& inst);
 void addConstraint(const IloExpr& expr);
};

struct CGModel {
  IloObjective cost;
  IloModel model;
  IloCplex cplex;

  //Variable
  NumVarMatrix lambda;

  // constraints
  IloRangeArray ClientServiced;
  IloRangeArray FacilityAssignment;
  IloArray<IloRangeArray> BranchConst; //B&P
  
  // coeficents constraints
  IloArray<NumNumMatrix> delta; // [n] x [#columns] x [m]

  // parameters for calculate coeficent objective
  NumNumMatrix qjS; //Gas-rate do compressor de cada coluna
  NumNumMatrix pjS; //Pressão do compressor em cada coluna
  NumNumMatrix cjS; //Valores objetivos de cada coluna (n * #c)

  NumNumMatrix relaxModelValues; //Storage the value of the variables from the optimal RMP relaxed
  //~ std::vector<std::vector<std::string> > relaxModelNames; //Storage the name of the variables from the optimal RMP relaxed
  
  CGModel(IloEnv& env) : cost(env), model(env), cplex(model){
	model.add(cost);	
  };
  //Create the model, defining the dimensions off the variables and coeficents
  void init(IloEnv& env, const int& compressors, const int& wells);
  //Create the set of initial columns
  void genInitRMP(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern);
  void genInitColumnManual(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern);
  void greedyIntialColumns(IloEnv& env, const Instance& inst);
  void greedyIntialColumns(IloEnv& env, const Instance& inst, std::unordered_map<std::string,int>& pattern) ;  
  void addConstraints(IloEnv& env);
  void addColumn(IloEnv env, const IloInt& j, const IloNum& qj, const IloNumArray& coef, const Instance& inst);
  int getStatus(const IloNum& n);
  double getPressure(const Instance& inst, const int& j, const double& qj);
  double costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC);
  bool cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time, const nodeBP& node, const double& UB);

  void findCloseIntegralityLevel1(IloEnv& env, const Instance& inst, double& varValue, int& compressor, IloNumArray sum_colJ); //For branch level 1
  void findCloseIntegralityLevel2(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well);   //For branch level 2
  
  //~ void findMostIntegralVariable(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well, const IloArray<pricing>& subproblem, const IloIntArray& restricWell);  
  //~ void findOtherSameCompressor(const Instance& inst,double& varValue, const int& compressor, const int& origCol, int& newCol, const IloIntArray& restricWell);
  void findOtherSameCompressor(const Instance& inst,double& varValue, const int& compressor, const int& origCol, int& newCol);
  void getColumnIndices(const IloNumVar& var, int& j, int& c, int& type);

  double primalHeuristic(const Instance& inst, IloEnv& env);
};


}



