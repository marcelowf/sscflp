#pragma once
#include <ilcplex/ilocplex.h>

namespace csp{
typedef IloArray<IloNumArray> NumNumMatrix;

void bPrice(std::string file, const int n_ini, const int n_fim);
void teste();
struct Column{
int j;
int c;	

Column(IloEnv& env){
	
	};	
};

struct nodeBP{
unsigned int type; //Type of node 1 or 2
int j;  //Index of the compressor of lambda selected to branch
int i;	//Index of the well (line of the column) that is supplied by the lambda
int rhs; //Storage the RHS value of the restriction (0 or 1)

IloArray<int> genJ; //Storage the index of compressor generated
IloArray<int> genC; //Storage the index of column generated

IloArray<int> colJ;  	//Storage the index of compressor in the node 
IloArray<int> colC;	//Storage the column
IloArray<int> colT;	//Storage the type of variable (0 - Slack / 1 - Normal)

IloIntArray restrictWell; //Storage the well that is restricted to only one compressor

IloArray<IloIntArray> ubPricing; //Storage the confirgurations of the UB in the pricing in this node

//~ IloArray<int> restrictedPricing;

nodeBP(IloEnv& env, const int& comp, const int& well, const int& rs, const unsigned int& tp):
	colJ(env), colC(env), colT(env), genJ(env), genC(env), ubPricing(env)
	
	{
	type = tp;
	j = comp;
	i = well;
	rhs = rs;
};


};
	 
}
