// Date: March/2015
// Description: Column Generation for Compressor Scheduling Problem
// Based on: "Column Generation for Solvin a Compressor Scheduling Problem" and "A revised Model for compressor design and scheduling in gas-lifted oil fields
// Eduardo Camponogara and Augustinho Plucenio
// Version with recalc of reduced cost - provide an exact objective value.
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
#define NDEBUG
#define NCOUT
#define NOptimalCols
#include <assert.h>

#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"
#define M 1.0e+6

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using csp::Instance;
using csp::CGModel;
using csp::pricing;


double CGModel::costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, 
				 const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC){
	  IloNum cost = cJ[compressor];
	  cost += IloScalProd(cIJ[compressor], delta);	  
	  return cost;
}
  		
void CGModel::init(IloEnv& env, const int& compressors, const int& wells){
    env.setNormalizer(IloFalse);
	cplex.setOut(env.getNullStream()); //Não mostrar iterações na tela.
	//~ cplex.setDeleteMode(IloCplex::LeaveBasis);
	//cplex.setParam(IloCplex::RootAlg, IloCplex::Dual);
	//~ cplex.setParam(IloCplex::NumericalEmphasis, 1);    //Precisão numérica	
	//~ cplex.setParam(IloCplex::AdvInd, 0); //
				
    lambda = NumVarMatrix(env,compressors); //Array de Array de variáveis [n]x[#col]      
    delta = IloArray<NumNumMatrix>(env, compressors);
    qjS = NumNumMatrix(env, compressors);
    cjS = NumNumMatrix(env, compressors);
    
    ClientServiced = IloRangeArray(env, wells, 1, 1);
    FacilityAssignment = IloRangeArray(env, compressors, 0, 1); 
    
    model.add(ClientServiced);
    model.add(FacilityAssignment);   
}

void pricing::init(IloEnv& env, const Instance& inst, const IloInt& j){
	//~ cplex.setParam(IloCplex::NumericalEmphasis, 1);    //Precisão numérica	
	//~ cplex.setParam(IloCplex::AdvInd, 0); //
	cplex.setParam(IloCplex::EpGap, 0.05);
	cplex.setOut(env.getNullStream());
	//~ cplex.setParam(IloCplex::RandomSeed, 123456789);
	int i;
	int n = inst.compressors;
	int m = inst.wells;
	
	//Variables
	x = IloNumVarArray(env, m, 0, 1, ILOINT);
	
	/*** Objective ***/
	IloExpr expr(env);
	expr.clear();
	for (i = 0; i < m; i++){		
		if (inst.orac[j][i] == 1) expr += x[i]; 
	}	
	SPj.setExpr(expr);
	model.add(SPj);
	
	/***Constraints***/
	//3d	
	model.add(IloScalProd(inst.qW,x) <= inst.qCMax[j]);	
}
void pricing::setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const Instance& inst){
	for (int i = 0; i < inst.wells ; i++){
		if (inst.orac[j][i] == 1){
			SPj.setLinearCoef(x[i], inst.cIJ[j][i]-piD[i]);
		}
	}
	SPj.setConstant(inst.cJ[j] - miD);
}


void CGModel::greedyIntialColumns(IloEnv& env, const Instance& inst){
	int i, j, c;
	for(j = 0; j < inst.compressors; j++){
		delta[j] = NumNumMatrix(env);
		delta[j].add(IloNumArray(env, inst.wells));
				
		cjS[j] = IloNumArray(env);
		cjS[j].add(inst.cJ[j]);
		
		qjS[j] = IloNumArray(env);
		qjS[j].add(0);
		
		lambda[j] = IloNumVarArray(env);
		IloNum qmin = 100.0;
		c = delta[j].getSize()-1;
		
		for (i = 0; i < inst.wells; i++){						
			if (inst.orac[j][i] == 1 && qjS[j][c]+inst.qW[i] <= inst.qCMax[j]){ 
				delta[j][c][i] = 1;
				qjS[j][c] += inst.qW[i];
				cjS[j][c] += inst.cIJ[j][i];
			}else if (inst.orac[j][i] == 1){
				delta[j].add(IloNumArray(env, inst.wells));
				qjS[j].add(inst.qW[i]);
				cjS[j].add(inst.cJ[j]+inst.cIJ[j][i]);
				
				c = delta[j].getSize()-1;
				delta[j][c][i] = 1;									
			}
		}
		// 1{3,4,12,13,16,} 2{1,2,7,17,} 3{} 4{5,10,14,} 5{} 6{8,9,15,18,} 7{} 8{6,11,}
		#ifndef NOptimalCols
		if (j == 0 || j == 1 || j == 3 || j == 5 || j == 7){
			delta[j].add(IloNumArray(env, inst.wells));
			c = delta[j].getSize()-1;
			cjS[j].add(inst.cJ[j]);
			if (j==0){
				delta[j][c][2] =1;
				delta[j][c][3] =1;
				delta[j][c][11] =1;
				delta[j][c][12] =1;
				delta[j][c][15] =1;				
			}
			if(j == 1){
				delta[j][c][0] =1;
				delta[j][c][1] =1;
				delta[j][c][6] =1;
				delta[j][c][16] =1;				
			}
			if (j == 3){
				delta[j][c][4] =1;
				delta[j][c][9] =1;
				delta[j][c][13] =1;				
			}
			if (j == 5){
				delta[j][c][7] =1;
				delta[j][c][8] =1;
				delta[j][c][14] =1;
				delta[j][c][17] =1;				
			}
			if (j == 7){
				delta[j][c][5] =1;				
				delta[j][c][10] =1;				
			}
			cjS[j][c] += IloScalProd(inst.cIJ[j], delta[j][c]);
		}		
		#endif
		
		//Slack variables (to ensure initial feasible solution for RMP)
		delta[j].add(IloNumArray(env, inst.wells));
		cjS[j].add(M);
		c = delta[j].getSize()-1; //'c' is the last index of the column (in the case, the slack)
		for(i = 0; i < inst.wells; i++){
			delta[j][c][i] = 1;
		}
		
		//Add var to the array lambda 
		for (int col = 0; col < c; col++){//Greedy columns
			//Create a name 'type*j_col'
			string s = "x1*" + to_string(j) + "_" + to_string(col);
			char const* name = s.c_str();
			//~ cout << s << " " << cjS[j][col] << delta[j][col] << inst.cJ[j] + IloScalProd(delta[j][col], inst.cIJ[j]) << " Costs" << inst.cIJ[j] << endl;
			//~ cjS[j][col] = costColum(inst.wells, delta[j][col], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][col], 0);
			lambda[j].add( cost(cjS[j][col]) + ClientServiced(delta[j][col]) + FacilityAssignment[j](1) );
			lambda[j][col].setName(name);
		}		
		//Slack
		lambda[j].add( cost(cjS[j][c]) +  ClientServiced(delta[j][c]) + FacilityAssignment[j](1));
		
		string s = "x0*" + to_string(j) + "_" + to_string(c);
		char const* name = s.c_str();
		//~ cout << s << " " << cjS[j][c] << delta[j][c] << inst.cJ[j] + IloScalProd(delta[j][c], inst.cIJ[j]) << " Costs" << inst.cIJ[j] << endl;				
		lambda[j][c].setName(name);		
		//Add to model - no make difference
		//~ model.add(lambda[j]);
	}

}

void CGModel::addColumn(IloEnv env, const IloInt& j, const IloNum& columnCost, const IloNumArray& coef, const Instance& inst){
	delta[j].add(IloNumArray(env, inst.wells));
	for(int i = 0; i < inst.wells; i++){
		delta[j][delta[j].getSize()-1][i] = coef[i];
	}
	cjS[j].add(columnCost);
	//~ cout << columnCost << endl;
	//Name construct
	string s =  "x1*" + to_string(j) + "_" + to_string(cjS[j].getSize()-1);
	char const* name = s.c_str();
	#ifndef NCOUT
	cout << "Add " << s << " " << coef << endl;
	#endif
	//Objective and constraint 5b/5c		
	lambda[j].add(cost(columnCost) + ClientServiced(coef) + FacilityAssignment[j](1) );
	
	//Add name	
	lambda[j][lambda[j].getSize()-1].setName(name);
}

int CGModel::getStatus(const IloNum& n){
	//* 1 if is fractional
	//* 2 if is integral.
	//* 3 if is infeasible (uses artificial variables)
	int j, c, result;
	result = 2;
	for(j = 0; j < n; j++){
		for(c = 0; c < lambda[j].getSize(); c++){
			string varName = lambda[j][c].getName();
			int varType = atoi(varName.substr(1, 1).c_str());
			if (varType == 0){ 
				if(cplex.getValue(lambda[j][c]) > RC_UB){
					result = 3;
					goto ex;
				}
			}
			if (cplex.getValue(lambda[j][c]) > RC_UB && cplex.getValue(lambda[j][c]) < NEAR_ONE){
				result = 1;				
			}
		}		
	}
	ex:	return result;
}

bool CGModel::cg_iter(IloEnv& env, const Instance& inst, IloArray<csp::pricing>& subproblem, Timer<chrono::milliseconds>& timer_ms, float& mp_time, float& p_time, const nodeBP& node){
	timer_ms.start();
	if(!cplex.solve()){ //This never happen in the first iteration of B&P because the slack variables
		mp_time += timer_ms.total();		
		return false;
	}
	mp_time += timer_ms.total();
	
	int cont; //Controla o nº de colunas adicionadas ao master
	NumNumMatrix coef(env, inst.compressors); 
	
	int i,j;
	int n = inst.compressors;
	int m = inst.wells;
	
	IloNumArray pi(env, inst.wells);
	IloNumArray mi(env, inst.compressors);		
	for(;;){
		//Get dual variables
		cplex.getDuals(pi, ClientServiced); //5b
		cplex.getDuals(mi, FacilityAssignment); //5c		
		double pricing_redCost = 0;
		cont = n;		
		for(j = 0; j < n; j++){
			coef[j] = IloNumArray(env, inst.wells);			
			subproblem[j].setDualCoef(j, mi[j], pi, inst);			
			//Execute pricing
			timer_ms.start();				
			if(subproblem[j].cplex.solve()){	
				p_time +=  timer_ms.total();					
				pricing_redCost = subproblem[j].cplex.getObjValue();
				//~ cout << "RC " << pricing_redCost << endl;
				//~ exit(0);
				if (pricing_redCost < RC_LB){						
					subproblem[j].cplex.getValues(subproblem[j].x, coef[j]);
					double columnCostP = inst.cJ[j] + IloScalProd(coef[j], inst.cIJ[j]);

					#ifndef NDEBUG
					double costRC = pricing_redCost + mi[j] + IloScalProd(coef[j], pi);
					assert(costRC == columnCostP);
					cout << j << " " << coef[j] << " Cost P " << columnCostP << " Cost Red_C " << costRC  << endl;
					#endif
					addColumn(env, j, columnCostP, coef[j], inst); //Coluna do Pricing
					//Storage the columns generated at this node
					node.genJ.add(j);
					node.genC.add(cjS[j].getSize()-1);
					cont--;						
				}
			}else{
				p_time +=  timer_ms.total();
			}			
		}
		if (cont == n){ //Caso nenhuma coluna seja adicionada, i.e. todos SPj são >=0
			break;			
		}else{
			timer_ms.start();
			cplex.solve();	//Never will be infeasible		
			mp_time +=  timer_ms.total();				
			//~ cplex.exportModel("master.lp");
		}
	}
	return true;
}

void csp::cg(string file, const int n_ini, const int n_fim) {
	stringstream out;
	out << 3 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	
	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime{0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
						
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);
						
			//Solve root node			
			nodeBP node(env, 0,0,0);			
			cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node);		
			
			totalTime += timer_global.total();
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << cgModel.cplex.getObjValue() << "\t";
			
			int cg_status = cgModel.getStatus(inst.compressors);
			if (cg_status == 1) output << "Feasible" << endl;
			else if (cg_status == 2) output << "Optimal" << endl;
			else output << "Infeasible" << endl;
			
		}
		catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
