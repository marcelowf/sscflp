//~ #include <queue>          
//~ #include <vector>         
//~ #include <stack>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
//~ #define NDEBUG
#include <assert.h>

#define NCOUT
#define NRMPZ			

#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"
#define M 1.0e+8

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using namespace csp;
			
int cuts = 0;
int nodes = 0;		

void CGModel::getColumnIndices(const IloNumVar& var, int& j, int& c, int& type){
string varName = var.getName();
	type = atoi(varName.substr(1,1).c_str());
	j = atoi(varName.substr(3, varName.find(DELIMITER)).c_str());
	c = atoi(varName.substr(varName.find(DELIMITER)+1).c_str());	
}

//To find other column of the same compressor when all lines set as 1 were restricted
void CGModel::findOtherSameCompressor(const Instance& inst, IloNum& varValue, const int& compressor, const int& origCol, int& newCol, const IloIntArray& restrictWell){
	#ifndef NCOUT
	cout  << "Original col: " << origCol << endl;
	#endif
	int type, col, j;
	for(int c = 0; c < lambda[compressor].getSize(); c++){
		double value = cplex.getValue(lambda[compressor][c]);
		//~ cout << c << "/" << lambda[compressor].getSize() << " : " << value << endl;
		if(value > RC_UB && value < NEAR_ONE){
			getColumnIndices(lambda[compressor][c], j, col, type);
			if(col != origCol){						
				for(int i = 0; i < inst.wells; i++){
					if (delta[compressor][col][i] >= RC_UB && restrictWell[i] == 0){
						newCol = col;					
						varValue = value;
						#ifndef NCOUT
						cout << "NEW COL " << col << endl;
						#endif
						goto founded;
					}
				}
			}
		}
	}
	founded: return;
}

void CGModel::findMostIntegralVariable(const Instance& inst, IloNum& varValue, int& compressor, int& well, const IloArray<pricing>& subproblem, const IloIntArray& restrictWell){
	int jMin=-1, jMax=-1;		//To identify the compressor of the most two 'integral' variable
	int j, c;					//Iterators
	int colMin, colMax, col, col1; 	//Indexes and idenficators of the column of 'j' above;
	int typeMin, typeMax, type; //Identificators of type of variable
	IloNum maxV = 0.5, minV = 0.5;
	for(IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		double value = cplex.getValue(it.getVar());
		if(value > RC_UB && value < NEAR_ONE){
			if (value >= maxV){ //If closer to 1
				maxV = value;					
				getColumnIndices(it.getVar(), jMax, colMax, typeMax);				
			}
			if (value < minV){ //If closer to 0
				minV = value;					
				getColumnIndices(it.getVar(), jMin, colMin, typeMin);
			}				
		}			
	}
	//Choose the more closer to integrality
	if (1 - minV > maxV){
		varValue = minV;
		compressor = jMin;
		col = colMin;
		type = typeMin;
	}else{
		varValue = maxV;
		compressor = jMax;
		col = colMax;
		type = typeMax;
	}

	
	col1 = col;
	//Find the well which is supplied by the 'compressor' in the column 'col' and is not restricted in the pricing yet
	findWell:		
	for (int i = 0; i < inst.wells; i++){			
		if(delta[compressor][col1][i] >= NEAR_ONE && restrictWell[i] == 0){
			well = i;
			goto fim;
		}
	}	
	//If no well was found	
	#ifndef NDEGUG
	cout << "NAO ACHOU POÇO: " << compressor << " " << col1 << endl;
	for (IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		int j, c, type;
		getColumnIndices(it.getVar(), j, c, type);
		if (j == compressor){
			if(cplex.getValue(it.getVar()) > RC_UB){							
				cout << compressor << " " << c << " coef:" << it.getCoef() << "*" << cplex.getValue(it.getVar()) << " : " << delta[j][c] << endl;
				//~ for (int i = 0; i < inst.wells; i++) cout << subproblem[compressor].x[i].getUB() << "  ";
			}
		}
	}
	cout << endl;
	#endif
	findOtherSameCompressor(inst, varValue, compressor, col, col1, restrictWell);
	goto findWell;
	fim: return;	
}

void Branch_Price(IloEnv &env, const Instance& inst, CGModel& cgModel, IloArray<pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,const nodeBP& node, IloNum& ub, NumNumMatrix& solution) {
	nodes++;
	//~ if (nodes == 261) exit(0);
	cout << "Node " << nodes ;
	#ifndef NCOUT
	cout << "Node " << nodes << endl;
	#endif
	
	if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node)){
		int cg_status = cgModel.getStatus(inst.compressors);
		//Infeasible (slack variables)
		if (cg_status == 3){
			#ifndef NCOUT
			cout << " Infeasible (artificial)" << endl;
			#endif
			cuts++;			
			return;
		}
		IloNum lb = cgModel.cplex.getObjValue();
		cout << " obj " << lb << " Status " << cg_status << " UB " << ub << endl;
		#ifndef NCOUT
		cout << " Solution obj " << lb << endl;
		double value = 0;
		for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
			if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
				int j, c, type;		
				cgModel.getColumnIndices(it.getVar(), j, c, type);				
				double costCjs = cgModel.cplex.getValue(it.getVar())*cgModel.cjS[j][c];
				double realCost = cgModel.cplex.getValue(it.getVar())*(inst.cJ[j]  +  IloScalProd(cgModel.delta[j][c], inst.cIJ[j]) );
				cout << it.getVar().getName() << "*coef : " << cgModel.cplex.getValue(it.getVar()) << "*";
				for (int i = 0; i < inst.wells; i++) cout << cgModel.delta[j][c][i] << ",";
				cout  << " = " << it.getCoef()*cgModel.cplex.getValue(it.getVar()) << " - Cost-cjS " << costCjs << " Real cost " <<  realCost << endl;
				for (int i = 0; i < inst.wells; i++) cout << subproblem[j].x[i].getUB() << " ";				
			}		
		}		
		for (int i = 0; i < inst.wells; i++) cout << " " << node.restrictWell[i] << " ";
		cout << endl;
		#endif
		//Integral
		if (cg_status == 2){ 			
			cuts++;
			#ifndef NCOUT
			cout << " Integral ";
			#endif
			//New best upper bound
			if(lb < ub){
				ub = lb;
				#ifndef NCOUT	
				cout << " New UB! " ;
				#endif
				///Clear the previous result and Storage the result
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
						//~ cout << "cjS[" << j<<"]["<<c<<"]" << cgModel.cjS[j][c] << " - Delta: " << cgModel.delta[j][c] << endl;
					}
				}
			}
			//~ cout << endl;
			return;
		}				
		 //If LB is worst than upper bound
		if (lb > ub){
			#ifndef NCOUT		
			cout << "worst than UB" << ub << endl;
			#endif
			cuts++;
			return;
		}		
		#ifndef NRMPZ	//Solve the RMP in integer form
		IloArray<IloConversion> conv(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){ 
			conv[j] = IloConversion(env, cgModel.lambda[j], ILOINT);
			cgModel.model.add(conv[j]);			
		}
		timer_ms.start();
		if(cgModel.cplex.solve()){
			mp_time += timer_ms.total();
			IloNum obj = cgModel.cplex.getObjValue();	
			if (obj < ub){
				ub = obj;
				///Clear the previous result and Storage the result
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
						//~ cout << "cjS[" << j<<"]["<<c<<"]" << cgModel.cjS[j][c] << " - Delta: " << cgModel.delta[j][c] << endl;
					}
				}			
			}						
		}		
		for (int j = 0; j < inst.compressors; j++){ //Undo the conversion
			cgModel.model.remove(conv[j]);						
		}
		timer_ms.start();
		cgModel.cplex.solve(); //Solve again to get the relaxed variables values TODO:Storage the values of relaxed model
		mp_time += timer_ms.total();
		#endif
	}else{//Infeasible
		#ifndef NCOUT
		cout << "Infeasible " << endl;
		#endif
		cuts++;
		return;
	}
	#ifndef NCOUT
	cout << " Fractional " << endl;
	#endif
	///Fractional soltion	
	//Find the lambda variable that is closer to integrality
	IloNum varValue;
	int compressor, well=-1;	
	cgModel.findMostIntegralVariable(inst, varValue, compressor, well, subproblem, node.restrictWell);	
	IloNumArray b(env, 2); 
	//Define the order of the RHS //TODO pegar a mais fracionária
	if (varValue > 0.5){
		b[0] = 0;
		b[1] = 1;
	}else{
		b[0] = 1;
		b[1] = 0;
	}		
	for (int i = 0; i < b.getSize(); i++){
		#ifndef NCOUT	
		cout << "Index " << i << " branch rule " << b[i] << endl;		
		cout << "(Before)Compressor " << compressor << " supply well " << well << " : " << b[i] << endl;
		#endif
		///Create the node 
		nodeBP nodeB(env, compressor, well, b[i]);
		
		nodeB.restrictWell = IloIntArray (node.restrictWell);
		if(b[i] == 1) nodeB.restrictWell[nodeB.i] = 1;

		//Copy the UB of the variables x of the previous node
		nodeB.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			nodeB.ubPricing[j] = IloIntArray(env, inst.wells);
			for (int id = 0; id < inst.wells; id++){
				nodeB.ubPricing[j][id] = node.ubPricing[j][id];
			}
		}
		for (int comp = 0; comp < inst.compressors; comp++){
			for (int col = cgModel.lambda[comp].getSize()-1; col >= 0 ; --col){
				int j, c, type;
				cgModel.getColumnIndices(cgModel.lambda[comp][col], j, c, type);					
				#ifndef NCOUT
				cout << "Variable " << cgModel.lambda[comp][col].getName() << " coef " << cgModel.cjS[j][c] << "*";
				for (int i = 0; i < inst.wells; i++) cout << cgModel.delta[j][c][i] << " ";
				#endif
				if (nodeB.j == j){//For j
					if(b[i] == 0){
						if(cgModel.delta[j][c][ nodeB.i ] > NEAR_ONE && type != 0){
							#ifndef NCOUT
							cout << " Removed ";
							#endif
							nodeB.colJ.add(j);
							nodeB.colC.add(c);
							nodeB.colT.add(type);
							cgModel.lambda[comp][col].end(); //Remove from model
							cgModel.lambda[comp].remove(col);//Remove from vector
						}
					}else{//b[i]==1
						if(cgModel.delta[j][c][ nodeB.i ] < RC_UB && type != 0){
							#ifndef NCOUT
							cout << " Removed " ;
							#endif
							nodeB.colJ.add(j);
							nodeB.colC.add(c);
							nodeB.colT.add(type);
							cgModel.lambda[comp][col].end();
							cgModel.lambda[comp].remove(col);
						}
					}
				}else{ 	//For N\j
					if(b[i] == 1){
						if(cgModel.delta[j][c][ nodeB.i ] > NEAR_ONE && type != 0){								
							#ifndef NCOUT
							cout << " Removed " ;
							#endif
							nodeB.colJ.add(j);
							nodeB.colC.add(c);
							nodeB.colT.add(type);
							cgModel.lambda[comp][col].end();
							cgModel.lambda[comp].remove(col);
						}	
					}				
				}
				#ifndef NCOUT
				cout << endl;
				#endif
			}		
		}			
		//Insert the pricing constraints and storage the modifications in the x variables in this node.
		for (int j = 0 ; j < inst.compressors; j++){
			if(b[i] == 0){
				if(j == nodeB.j){
					subproblem[j].x[nodeB.i].setUB(0);
					nodeB.ubPricing[j][nodeB.i] = 0;
				}
			}else{ //b[i]==1
				if(j == nodeB.j){
					if(j == nodeB.j){
						subproblem[j].x[nodeB.i].setLB(1);
					}					
				}else if(node.ubPricing[j][nodeB.i] == 1){ //The ub is set to 0 and stored in ubPricing only if in the previos x.ub was 1
					subproblem[j].x[nodeB.i].setUB(0);
					nodeB.ubPricing[j][nodeB.i] = 0;
				}
			}
		}	
		///Branch and price recursive
		Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, nodeB, ub, solution);		
		#ifndef NCOUT
		cout << "(After)Compressor " << compressor << " supply well " << well << " : " << b[i] << endl;		
		#endif
		//Re-insert the infeasible variables of the node
		for (int k = 0; k < nodeB.colJ.getSize(); k++){
			cgModel.lambda[ nodeB.colJ[k] ].add(
			cgModel.cost(cgModel.cjS[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Cost
			cgModel.ClientServiced(cgModel.delta[ nodeB.colJ[k] ][ nodeB.colC[k] ]) + //Constraint B
			cgModel.FacilityAssignment[ nodeB.colJ[k] ](1) );  //Constraint C
			
			//Name
			string s =  "x" + to_string(nodeB.colT[k]) + "*" + to_string(nodeB.colJ[k]) + "_" + to_string(nodeB.colC[k]);
			#ifndef NCOUT
			cout << "Re-add " << s << endl;
			#endif
			char const* name = s.c_str();
			cgModel.lambda[ nodeB.colJ[k] ][cgModel.lambda[nodeB.colJ[k]].getSize()-1].setName(name);
		}
		
		#ifndef NCOUT
		cout << "Variables to remove: " << nodeB.genC.getSize() << endl;
		#endif
			
		//Remove restrictions of the pricing in j
		if (b[i] == 0){
			subproblem[nodeB.j].x[nodeB.i].setUB(1);
			nodeB.ubPricing[nodeB.j][nodeB.i] = 1;
		}else{//b[i]=1
			nodeB.restrictWell[nodeB.i] = 0;
			subproblem[nodeB.j].x[nodeB.i].setLB(0);
		}
		int nRemoved = 0;
		for (int comp = 0; comp < inst.compressors; comp++){
			//Remove restrictions of the pricing in N\j 
			if (b[i] == 1){
				if(comp != nodeB.j && node.ubPricing[comp][nodeB.i] >= NEAR_ONE){
					subproblem[comp].x[nodeB.i].setUB(1);
					nodeB.ubPricing[comp][nodeB.i] = 1;					
				}
			}
			//Remove the variables generated at this node -- TODO Improve this
			for (int col = cgModel.lambda[comp].getSize()-1; col >= 0 ; --col){
				int j, c, type;
				cgModel.getColumnIndices(cgModel.lambda[comp][col], j, c, type);
				for (int id = nodeB.genC.getSize()-1; id >= 0 ; --id){
					if (nodeB.genJ[id] == j && nodeB.genC[id] == c){
						#ifndef NCOUT
						cout << cgModel.lambda[comp][col].getName() << " coef " << cgModel.cjS[j][c] << endl;
						#endif
						cgModel.lambda[comp][col].end(); //Remove from model
						cgModel.lambda[comp].remove(col);//Remove from vector
						nRemoved++;
						//~ nodeB.genJ.remove(id);
						//~ nodeB.genC.remove(id);
						break;
					}
				}			
			}
		}
		#ifndef NDEBUG
		assert(nRemoved == nodeB.genC.getSize());
		#endif		
	}
}

void csp::bPrice(string file, const int n_ini, const int n_fim){
	stringstream out;
	out << 4 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	
	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj\t" <<  "Nodes\t" << "Cuts\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime {0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
				
		IloNum ub = {numeric_limits<double>::max()};			
		cuts = 0;
		nodes = 0;		
		NumNumMatrix solution(env,inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			solution[j] = IloNumArray(env, inst.wells);
		}
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);				

			nodeBP node(env,0,0,0);
			//~ node.restrictedPricing = IloArray<int>(env,inst.compressors);
			node.restrictWell = IloIntArray(env, inst.wells);
			node.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
			for (int i = 0; i < inst.compressors; i++){
				node.ubPricing[i] = IloIntArray(env, inst.wells);
				for (int j = 0; j < inst.wells; j++){
					node.ubPricing[i][j] = 1;
				}
			 }
			//BEGIN OF RECURSION			
			Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, node, ub, solution);
			totalTime += timer_global.total();
			
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << ub << "\t" << nodes << "\t" << cuts << "\t";
			double columnCost = 0;
			for (int j = 0; j < inst.compressors; j++){
				output << j+1 << "{";
				int ativo = 0;
				for (int i = 0; i < inst.wells; i++){					
					if (solution[j][i] >= NEAR_ONE){
						 output << i+1 << ",";
						 columnCost += inst.cIJ[j][i];//*solution[j][i];
						 //~ output << "[" << inst.cIJ[j][i] << "]";
						 ativo++;
					}
				}
				if (ativo > 0){
					 columnCost += inst.cJ[j];
					 //~ output << "[" << inst.cJ[j] << "]";
				}	
				output << "}";
			}
			
			output << " - COST: " << columnCost << endl;
			
		}catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
