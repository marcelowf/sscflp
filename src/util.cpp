// Copyright 2012 Leonardo Moura
#include <cmath>
#include <iostream>

#include "../include/util.h"
#include "../include/cg.h"
using namespace std;
using std::chrono::high_resolution_clock;
using csp::Instance;

template <typename T>
double truncateBetween(T x, T xmin, T xmax) {
  return max(min(x, xmax), xmin);
}

template <typename T>
Timer<T>::Timer() : start_(high_resolution_clock::now()),
    running_(true)  { }

template <typename T>
Timer<T>::Timer(Timer<T>::R time_limit) : start_(high_resolution_clock::now()),
    running_(true), time_limit_(time_limit) { }

template <typename T>
void Timer<T>::start() { start_ = high_resolution_clock::now(); running_ = true; }

template <typename T>
void Timer<T>::stop() { finish_ = high_resolution_clock::now(); running_ = false; }

template <typename T>
typename Timer<T>::R Timer<T>::total() const {
  if(running_)
    return std::chrono::duration_cast<T>(high_resolution_clock::now() - start_).count();
  else
    return std::chrono::duration_cast<T>(finish_ - start_).count();
}

template <typename T>
bool Timer<T>::reachedTimeLimit() const {
  return total() > time_limit_;
}

template <typename T>
bool Timer<T>::isRunning() const {
  return running_;
}

template class Timer<std::chrono::milliseconds>;
template class Timer<std::chrono::seconds>;

void Instance::readFile(IloEnv& env, ifstream& input){
	cJ = IloNumArray(env); 
	dJ = IloNumArray(env); 
	qCMin = IloNumArray(env); 
	qCMax = IloNumArray(env); 
	alpha0 = IloNumArray(env); 
	alpha1 = IloNumArray(env); 
	alpha2 = IloNumArray(env); 
	alpha3 = IloNumArray(env); 
	alpha4 = IloNumArray(env); 
	pointsQ = NumNumMatrix(env); 
	pointsH = NumNumMatrix(env);

	qW = IloNumArray(env); 
	pW = IloNumArray(env); 

	orac = NumNumMatrix(env); 
	cIJ = NumNumMatrix(env); 
	lIJ = NumNumMatrix(env);  
	qMaxIJ = NumNumMatrix(env); 

	string s;
	IloNum c, d;
	IloInt temp, x;
	
	//Parameters
	input >> compressors >> wells >> k;	
	
	/*****Scan file*****/
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}	
	//Compressor data	
	for (int i = 0; i < compressors; i++){
		pointsQ.add(IloNumArray(env, k));
		pointsH.add(IloNumArray(env, k));		
		
		cIJ.add(IloNumArray(env, wells));		
		lIJ.add(IloNumArray(env, wells));		
		qMaxIJ.add(IloNumArray(env, wells));
		orac.add(IloNumArray(env, wells));	
		
		double compCost, energyCost, rateMin, rateMax, a0, a1, a2, a3, a4;
		input >> temp >> compCost >> energyCost >> rateMin >> rateMax >> a0 >> a1 >> a2 >> a3 >> a4;
		cJ.add(compCost);
		dJ.add(energyCost);
		qCMin.add(rateMin);
		qCMax.add(rateMax);
		alpha0.add(a0);
		alpha1.add(a1);
		alpha2.add(a2);
		alpha3.add(a3);
		alpha4.add(a4);
		
	}
	//Well data
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}
	for (int i = 0; i < wells; i++){		
		double gasCli, pCli;
		input >> temp >> gasCli >> pCli; //percorre até pw
		qW.add(gasCli);
		pW.add(pCli);
		
		getline(input, s); //pega o Ni
		stringstream ss(s); //Define a string como stream
		while(ss >> c){   //Take the doubles
			orac[c-1][i] = 1;						
		}
	}
	//Pipeline
	for (int i = 0; i < 2; i++){
		getline(input, s);
	}
	//Custo alto para itens fora de orac
	//~ for (int j = 0; j < compressors; j++){
		//~ for (int i = 0; i < wells; i++){
			//~ lIJ[j][i] = 9999999;
			//~ cIJ[j][i] = 9999999;
			//~ qMaxIJ[j][i] = 9999999;
		//~ }
	//~ }	
	while(getline(input, s)){
		if(s == "Service cost")break;
		stringstream(s) >> temp >> x >> c;
		lIJ[x-1][temp-1] = c;		
	}
	
	//Service cost
	getline(input, s);
	while(getline(input, s)){
		if(s == "Points")break;
		stringstream(s) >> temp >> x >> c;
		cIJ[x-1][temp-1] = c; 				
	}
	//Points
	getline(input, s);
	while(getline(input, s)){
		if(s == "Max output gas-rate")break;
		double p;
		stringstream(s) >> temp >> x >> c >> p;
		pointsQ[temp-1][x] = c; 
		pointsH[temp-1][x] = dJ[temp-1]*c*p;				
	}	
	//Max output gas-rate
	getline(input, s);
	while(getline(input, s)){
		stringstream(s) >> temp >> x >> c;
		qMaxIJ[x-1][temp-1] = c;		
	}	
	
	//Print read instance
	//~ cout << "j\t cj\t qcMax " << endl;
	//~ for (int j = 0; j < compressors ; j++){
		//~ cout << j << "\t" << cJ[j] << "\t" << qCMax[j] << endl;	
	//~ }
	//~ cout << "j\t" << "i\t" << "cIJ" << endl;
	//~ for (int j = 0; j < compressors ; j++){
		//~ for (int i = 0; i < wells; i++){
				//~ cout << j << "\t" << i << "\t" << cIJ[j][i] << endl;
		//~ }
	//~ }
}

double fRand(double fMin, double fMax,const int& seed){
	srand(seed);
	double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}
int iRand(int fMin, int fMax,const int& seed){
	srand(seed);
	return rand() % fMax + fMin;
}
