//~ #include <queue>          
//~ #include <vector>         
//~ #include <stack>
#include <limits>
#include <math.h>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
#define NDEBUG
#include <assert.h>

//~ #define NCOUT
//~ #define NRMPZ			

#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"
#define M 1.0e+8

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;
typedef std::numeric_limits< double > dbl;


using namespace std;
using namespace csp;
			
int cuts = 0;
int nodes = 0;
double lowerB = 0;
double DecPrecision = 1.0e+6;

void CGModel::getColumnIndices(const IloNumVar& var, int& j, int& c, int& type){
string varName = var.getName();
	type = atoi(varName.substr(1,1).c_str());
	j = atoi(varName.substr(3, varName.find(DELIMITER)).c_str());
	c = atoi(varName.substr(varName.find(DELIMITER)+1).c_str());	
}

//To find other column of the same compressor when all lines set as 1 were restricted
void CGModel::findOtherSameCompressor(const Instance& inst, double& varValue, const int& compressor, const int& origCol, int& newCol, const IloIntArray& restrictWell){
	#ifndef NCOUT
	//~ if (nodes > 178990)
	cout  << "Original col: " << origCol << endl;
	#endif
	int type, col, j;
	for(int c = 0; c < lambda[compressor].getSize(); c++){
		double value = cplex.getValue(lambda[compressor][c]);
		//~ cout << c << "/" << lambda[compressor].getSize() << " : " << value << endl;
		if(value > RC_UB && value < NEAR_ONE){
			getColumnIndices(lambda[compressor][c], j, col, type);
			if(col != origCol){						
				for(int i = 0; i < inst.wells; i++){
					if (delta[compressor][col][i] >= RC_UB && BranchConst[j][i].getLB() == 0){
						newCol = col;					
						varValue = value;
						#ifndef NCOUT		
						//~ if (nodes > 178990)
						cout << "NEW COL " << col << endl;
						#endif
						goto founded;
					}
				}
			}
		}
	}
	founded: return;
}

void CGModel::findMostIntegralVariable(const Instance& inst, double& varValue, int& compressor, int& well, const IloArray<pricing>& subproblem, const IloIntArray& restrictWell){
	int jMin=-1, jMax=-1;		//To identify the compressor of the most two 'integral' variable
	int j, c;					//Iterators
	int colMin, colMax, col, col1; 	//Indexes and idenficators of the column of 'j' above;
	int typeMin, typeMax, type; //Identificators of type of variable
	double maxV = 0.5, minV = 0.5;
	for(IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		double value = cplex.getValue(it.getVar());
		if(value >= RC_UB && value <= NEAR_ONE){			
			cout << it.getVar().getName() << " * " << value << " is fractional ";
			if (value >= maxV){ //If closer to 1				
				cout << "and is closer to 1 (" << value << " >= " << maxV;
				maxV = value;					
				getColumnIndices(it.getVar(), jMax, colMax, typeMax);				
			}
			else if (value <= minV){ //If closer to 0
				cout << "and is closer to 0 (" << value << " < " << minV;
				minV = value;					
				getColumnIndices(it.getVar(), jMin, colMin, typeMin);
			}
			cout << endl;				
		}			
	}
	//Choose the more closer to integrality (with precision of 6 decimal cases)
	//double roundValue = floor(value * DecPrecision) / DecPrecision;	
	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) > (floor(maxV * DecPrecision) / DecPrecision)){
		#ifndef NCOUT
		//~ if (nodes > 178990)
		cout << "Selected most closer to 0" << endl;
		#endif
		varValue = minV;
		compressor = jMin;
		col = colMin;
		type = typeMin;
	}else{
		#ifndef NCOUT
		//~ if (nodes > 178990)
		cout << "Selected most closer to 1" << endl;
		#endif
		varValue = maxV;
		compressor = jMax;
		col = colMax;
		type = typeMax;
	}
	#ifndef NCOUT
	//~ if (nodes > 178990)
	cout << "most close to integrality: " << compressor << "_" << col << endl;
	#endif
	col1 = col;
	//Find the well which is supplied by the 'compressor' in the column 'col' and is not restricted in the pricing yet
	findWell:		
	for (int i = 0; i < inst.wells; i++){			
		if(delta[compressor][col1][i] >= NEAR_ONE && BranchConst[compressor][i].getLB() < NEAR_ONE){
			well = i;
			goto fim;
		}
	}	
	//If no well was found	
	#ifndef NCOUT
	//~ if (nodes > 178990){
	cout << "NAO ACHOU POÇO: " << compressor << " " << col1 << endl;
	for (IloExpr::LinearIterator it = cost.getLinearIterator(); it.ok(); ++it){
		int j, c, type;
		getColumnIndices(it.getVar(), j, c, type);
		if (j == compressor){
			if(cplex.getValue(it.getVar()) > RC_UB){							
				cout << compressor << " " << c << " coef:" << it.getCoef() << "*" << cplex.getValue(it.getVar()) << " : " << delta[j][c] << endl;
				//~ for (int i = 0; i < inst.wells; i++) cout << subproblem[compressor].x[i].getUB() << "  ";
			}
		}
	}
	cout << endl;
	//~ }
	#endif
	findOtherSameCompressor(inst, varValue, compressor, col, col1, restrictWell);
	goto findWell;
	fim: return;	
}

void Branch_Price(IloEnv &env, const Instance& inst, CGModel& cgModel, IloArray<pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,const nodeBP& node, IloNum& ub, NumNumMatrix& solution) {
	nodes++;	
	cout << nodes << "\t";
	#ifndef NCOUT
	//~ if (nodes > 178990)
	cout << "Node " << nodes << endl;
	#endif
	
	if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node, ub)){
		int cg_status = cgModel.getStatus(inst.compressors);
		//Infeasible (slack variables)
		if (cg_status == 3){
			cout << ub << endl;
			#ifndef NCOUT
			//~ if (nodes > 178990)
			cout << " Infeasible (artificial)" << endl;
			#endif
			cuts++;			
			return;
		}
		IloNum lb = cgModel.cplex.getObjValue();
		
		//~ cout << " obj " << lb << " Status " << cg_status << " UB " << ub << endl;
		cout << lb << endl;		
		#ifndef NCOUT
		//~ if (nodes > 178990){
		cout << " Solution obj " << lb << endl;
		double value = 0;
		for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
			if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
				int j, c, type;		
				cgModel.getColumnIndices(it.getVar(), j, c, type);				
				double costCjs = cgModel.cplex.getValue(it.getVar())*cgModel.cjS[j][c];
				double realCost = cgModel.cplex.getValue(it.getVar())*(inst.cJ[j]  +  IloScalProd(cgModel.delta[j][c], inst.cIJ[j]) );
				cout << it.getVar().getName() << "*coef : " << cgModel.cplex.getValue(it.getVar()) << "*";
				for (int i = 0; i < inst.wells; i++) cout << cgModel.delta[j][c][i] << ",";
				cout  << " = " << it.getCoef()*cgModel.cplex.getValue(it.getVar()) << " - Cost-cjS " << costCjs << " Real cost " <<  realCost << endl;
				for (int i = 0; i < inst.wells; i++) cout << subproblem[j].x[i].getUB() << " ";				
			}		
		}		
		//~ for (int i = 0; i < inst.wells; i++) cout << " " << node.restrictWell[i] << " ";
		cout << endl;
		//~ }
		#endif
		//Integral
		if (cg_status == 2){ 			
			cuts++;
			#ifndef NCOUT
			//~ if (nodes > 178990)
			cout << " Integral ";
			#endif
			//New best upper bound
			if(lb < ub){
				ub = lb;
				#ifndef NCOUT
				//~ if (nodes > 178990)	
				cout << " New UB! " ;
				#endif
				///Clear the previous result and Storage the result
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
						//~ cout << "cjS[" << j<<"]["<<c<<"]" << cgModel.cjS[j][c] << " - Delta: " << cgModel.delta[j][c] << endl;
					}
				}
			}
			//~ cout << endl;
			return;
		}				
		 //If LB is worst than upper bound
		if (lb >= ub){
			#ifndef NCOUT
			//~ if (nodes > 178990)	
			cout << "worst than UB" << ub << endl;
			#endif
			cuts++;
			return;
		}

		#ifndef NRMPZ	//Solve the RMP in integer form		
		IloArray<IloConversion> conv(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){ 
			conv[j] = IloConversion(env, cgModel.lambda[j], ILOINT);
			cgModel.model.add(conv[j]);			
		}		
		timer_ms.start();
		if(cgModel.cplex.solve()){		
			mp_time += timer_ms.total();
			IloNum obj = cgModel.cplex.getObjValue();	
			if (obj < ub){
				ub = obj;
				///Clear the previous result and Storage the result
				for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						int j, c, type;		
						cgModel.getColumnIndices(it.getVar(), j, c, type);
						solution[j] = cgModel.delta[j][c];						
						//~ cout << "cjS[" << j<<"]["<<c<<"]" << cgModel.cjS[j][c] << " - Delta: " << cgModel.delta[j][c] << endl;
					}
				}			
			}						
		}		
		for (int j = 0; j < inst.compressors; j++){ //Undo the conversion
			cgModel.model.remove(conv[j]);						
		}
		timer_ms.start();
		cgModel.cplex.solve(); //Solve again to get the relaxed variables values TODO:Storage the values of relaxed model
		mp_time += timer_ms.total();
		#endif
	}else{//Infeasible or LB is worst than UB
		#ifndef NCOUT
		//~ if (nodes > 178990)
		cout << "Infeasible " << endl;
		#endif
		cuts++;
		cout << ub << endl;
		return;
	}
	#ifndef NCOUT
	//~ if (nodes > 178990)
	cout << " Fractional " << endl;
	#endif
	///Fractional soltion	
	//Find the lambda variable that is closer to integrality
	IloNum varValue;
	int compressor, well=-1;	
	cgModel.findMostIntegralVariable(inst, varValue, compressor, well, subproblem, node.restrictWell);	
	IloNumArray b(env, 2); 
	//Define the order of the RHS //TODO pegar a mais fracionária
	if (varValue > 0.5){
		b[0] = 0;
		b[1] = 1;
	}else{
		b[0] = 1;
		b[1] = 0;
	}		
	for (int i = 0; i < b.getSize(); i++){
		#ifndef NCOUT
		//~ if (nodes > 178990)
		cout << "Index " << i << " branch rule " << compressor << " supply well " << well << " : " << b[i] << endl;
		#endif
		///Create the node 
		nodeBP nodeB(env, compressor, well, b[i]);

		//Copy the UB of the variables x of the previous node
		nodeB.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			nodeB.ubPricing[j] = IloIntArray(env, inst.wells);
			for (int id = 0; id < inst.wells; id++){
				nodeB.ubPricing[j][id] = node.ubPricing[j][id];
			}
		}

		//Insert the pricing constraints and storage the modifications in the x variables in this node.
		for (int j = 0 ; j < inst.compressors; j++){
			if(b[i] == 0){
				if(j == nodeB.j){
					cgModel.BranchConst[j][nodeB.i].setUB(0); //RMP
					subproblem[j].x[nodeB.i].setUB(0); 		//Pricing
					nodeB.ubPricing[j][nodeB.i] = 0;		
				}
			}else{ //b[i]==1
				if(j == nodeB.j){
					cgModel.BranchConst[j][nodeB.i].setLB(1); //RMP
					subproblem[j].x[nodeB.i].setLB(1); //Pricing								
				}else if(node.ubPricing[j][nodeB.i] == 1){ //The ub is set to 0 and stored in ubPricing only if in the previos x.ub was 1
					subproblem[j].x[nodeB.i].setUB(0); //Pricing
					nodeB.ubPricing[j][nodeB.i] = 0;   //    
				}
			}
		}	
		
		///Branch and price recursive
		Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, nodeB, ub, solution);		

		//Remove restrictions of the pricing in j
		if (b[i] == 0){
			cgModel.BranchConst[nodeB.j][nodeB.i].setUB(1);
			subproblem[nodeB.j].x[nodeB.i].setUB(1);
			nodeB.ubPricing[nodeB.j][nodeB.i] = 1;
		}else{//b[i]=1
			cgModel.BranchConst[nodeB.j][nodeB.i].setLB(0);
			subproblem[nodeB.j].x[nodeB.i].setLB(0);			
		}
		
		for (int comp = 0; comp < inst.compressors; comp++){
			//Remove restrictions of the pricing in N\j 
			if (b[i] == 1){
				if(comp != nodeB.j && node.ubPricing[comp][nodeB.i] >= NEAR_ONE){
					subproblem[comp].x[nodeB.i].setUB(1);
					nodeB.ubPricing[comp][nodeB.i] = 1;					
				}
			}
		}
	
	}
}

void csp::bPrice(string file, const int n_ini, const int n_fim){
	stringstream out;
	cout.precision(dbl::max_digits10);
	out << 4 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}

	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj\t" <<  "Nodes\t" << "Cuts\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime {0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
				
		IloNum ub = {numeric_limits<double>::max()};			
		cuts = 0;
		nodes = 0;		
		NumNumMatrix solution(env,inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			solution[j] = IloNumArray(env, inst.wells);
		}
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);				

			nodeBP node(env,0,0,0);
			//~ node.restrictedPricing = IloArray<int>(env,inst.compressors);
			node.restrictWell = IloIntArray(env, inst.wells);
			node.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
			for (int i = 0; i < inst.compressors; i++){
				node.ubPricing[i] = IloIntArray(env, inst.wells);
				for (int j = 0; j < inst.wells; j++){
					node.ubPricing[i][j] = 1;
				}
			 }
			//BEGIN OF RECURSION			
			Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, node, ub, solution);
			totalTime += timer_global.total();
			
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << ub << "\t" << nodes << "\t" << cuts << "\t";
			double columnCost = 0;
			for (int j = 0; j < inst.compressors; j++){
				output << j+1 << "{";
				int ativo = 0;
				for (int i = 0; i < inst.wells; i++){					
					if (solution[j][i] >= NEAR_ONE){
						 output << i+1 << ",";
						 columnCost += inst.cIJ[j][i];//*solution[j][i];
						 //~ output << "[" << inst.cIJ[j][i] << "]";
						 ativo++;
					}
				}
				if (ativo > 0){
					 columnCost += inst.cJ[j];
					 //~ output << "[" << inst.cJ[j] << "]";
				}	
				output << "}";
			}
			
			output << " - COST: " << columnCost << endl;
			
		}catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
