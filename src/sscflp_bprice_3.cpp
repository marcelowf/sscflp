#include <queue>          
#include <vector>
#include <limits>
#include <math.h>
#include "../include/cg.h"
#include "../include/util.h"
#include "../include/bprice.h"
#define NDEBUG
#include <assert.h>

#define NCOUT
//~ #define NRMPZ			

#define RC_LB -1.0e-6
#define RC_UB 1.0e-6
#define NEAR_ONE 0.999999
#define DELIMITER "_"
#define M 1.0e+8

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;
typedef std::numeric_limits< double > dbl;

using namespace std;
using namespace csp;
			
int cuts = 0;
int nodes = 0;
double DecPrecision = 1.0e+6;

void CGModel::getColumnIndices(const IloNumVar& var, int& j, int& c, int& type){
	string varName = var.getName();
	type = atoi(varName.substr(1,1).c_str());
	j = atoi(varName.substr(3, varName.find(DELIMITER)).c_str());
	c = atoi(varName.substr(varName.find(DELIMITER)+1).c_str());	
}

void CGModel::findCloseIntegralityLevel1(IloEnv& env, const Instance& inst, double& varValue, int& compressor, IloNumArray sum_colJ){
	int jMin=-1, jMax=-1;
	int j, c;
	double maxV = 0.5, minV = 0.5;
	//Define the 'j's with value most clote to 1 and 0
	for(j = 0; j < inst.compressors; j ++){
		if (sum_colJ[j] < NEAR_ONE && sum_colJ[j] > RC_UB){
			if (sum_colJ[j] >= maxV){ //Closer to 1
				maxV = sum_colJ[j];
				jMax = j;
			}else if (sum_colJ[j] <= minV){//Closer to 0
				minV = sum_colJ[j];
				jMin = j;
			}
		}
	}
	//Define between jMin and jMax the most close to integrality
	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) > (floor(maxV * DecPrecision) / DecPrecision)){
		#ifndef NCOUT		
		cout << "Selected most closer to 0: " << jMin << endl;
		#endif
		varValue = minV;
		compressor = jMin;	
	}else{
		#ifndef NCOUT
		cout << "Selected most closer to 1: " << jMax << endl;
		#endif
		varValue = maxV;
		compressor = jMax;		
	}
	#ifndef NCOUT
	cout << "most close to integrality: " << compressor << endl;
	#endif	
}

//To find other column of the same compressor when all lines set as 1 were restricted
void CGModel::findOtherSameCompressor(const Instance& inst, double& varValue, const int& compressor, const int& origCol, int& newCol){
	#ifndef NCOUT
	cout  << "Original col: " << origCol << endl;
	#endif
	//~ int type, col, j;
	for(int c = 0; c < relaxModelValues[compressor].getSize(); c++){
		if(relaxModelValues[compressor][c] > RC_UB && relaxModelValues[compressor][c] < NEAR_ONE){
			if(c != origCol){						
				for(int i = 0; i < inst.wells; i++){
					if (delta[compressor][c][i] >= RC_UB && BranchConst[compressor][i].getLB() == 0){
						newCol = c;					
						varValue = relaxModelValues[compressor][c];
						#ifndef NCOUT		
						cout << "NEW COL " << c << endl;
						#endif
						goto founded;
					}
				}
			}
		}
	}
	founded: return;
}

void CGModel::findCloseIntegralityLevel2(IloEnv& env, const Instance& inst, double& varValue, int& compressor, int& well){
	int jMin=-1, jMax=-1;			//To identify the compressor of the most two 'integral' variable
	int j, c;						//Iterators
	int colMin, colMax, col, col1; 	//Indexes and idenficators of the column of 'j' above;
	int typeMin, typeMax, type; 	//Identificators of type of variable
	double maxV = 0.5, minV = 0.5;	
	for(j = 0; j < inst.compressors; j++){
		for(c = 0; c < relaxModelValues[j].getSize(); c++){			
			if(relaxModelValues[j][c] >= RC_UB && relaxModelValues[j][c] <= NEAR_ONE){			
				//~ cout << j << "_" << c << " * " << relaxModelValues[j][c] << " is fractional ";
				if (relaxModelValues[j][c] >= maxV){ //If closer to 1				
					//~ cout << "and is closer to 1 (" << relaxModelValues[j][c] << " >= " << maxV;
					maxV = relaxModelValues[j][c];					
					jMax = j;	
					colMax = c;	//Assuming that	the index of columns are the same in lambda and delta/cJS
				}
				else if (relaxModelValues[j][c] <= minV){ //If closer to 0
					//~ cout << "and is closer to 0 (" << relaxModelValues[j][c] << " <= " << minV;
					minV = relaxModelValues[j][c];					
					jMin = j;
					colMin = c;					
				}
				//~ cout << endl;				
			}			
		}	
	}
	if (1.0 - (floor(minV * DecPrecision) / DecPrecision) > (floor(maxV * DecPrecision) / DecPrecision)){
		#ifndef NCOUT
		cout << "Selected most closer to 0" << endl;
		#endif
		varValue = minV;
		compressor = jMin;
		col = colMin;
		type = typeMin;
	}else{
		#ifndef NCOUT
		cout << "Selected most closer to 1" << endl;
		#endif
		varValue = maxV;
		compressor = jMax;
		col = colMax;
		type = typeMax;
	}
	#ifndef NCOUT
	cout << "most close to integrality: " << compressor << "_" << col << endl;
	#endif
	col1 = col;
	//Find the well which is supplied by the 'compressor' in the column 'col' and is not restricted in the pricing yet
	findWell:		
	for (int i = 0; i < inst.wells; i++){			
		if(delta[compressor][col1][i] >= NEAR_ONE && BranchConst[compressor][i].getLB() < NEAR_ONE){
			well = i;
			goto fim;
		}
	}	
	
	//If no well was found	
	#ifndef NCOUT
	cout << "NAO ACHOU POÇO: " << compressor << " " << col1 << endl;
	#endif
	findOtherSameCompressor(inst, varValue, compressor, col, col1);
	goto findWell;
	fim: return;	
}
		

double CGModel::primalHeuristic(const Instance& inst, IloEnv& env){	
	struct Column{
		int id;
		double costB;
	};

	struct CompareColumn{
		public:
		bool operator()(const Column& c1,const Column& c2){
			return (c2.costB < c1.costB);
		}
	};
	//Vector of wells supplied (-1 defaul, otherwise storage the id of the compressor)	
	double objValue = 0;
	IloNumArray wellSuppli(env, inst.wells);
	int unsuppliedWells = inst.wells;
	
	for (int i = 0; i < inst.wells; i++) wellSuppli[i] = -1;

	IloArray<priority_queue<Column,vector<Column>, CompareColumn> >pQs(env, inst.compressors);
	for (int j = 0 ; j < inst.compressors; j++){
		//Storage the columns by their cost/nº of clients
		for (int c = 0; c < lambda[j].getSize(); c++){
			Column col{	c, (cjS[j][c]/IloSum(delta[j][c]))};			
			pQs[j].push(col);
		}
	}
	
	return 0;
}

void Branch_Price(IloEnv &env, const Instance& inst, CGModel& cgModel, IloArray<pricing>& subproblem, Timer<std::chrono::milliseconds>& timer_ms, float& mp_time, float& p_time,const nodeBP& node, IloNum& ub, NumNumMatrix& solution) {
	nodes++;
	IloNumArray sum_colJ(env, inst.compressors); //Storage the sum on variables y of the relaxed model
	cout << nodes << "\t";
	#ifndef NCOUT
	cout << "Node " << nodes << endl;
	#endif	
	if(cgModel.cg_iter(env, inst, subproblem, timer_ms, mp_time, p_time, node, 1000)){
		//Define a feasible solution
		//~ ub = cgModel.primalHeuristic(inst, env);
		int cg_status = cgModel.getStatus(inst.compressors);
		//Infeasible (slack variables)
		if (cg_status == 3){
			#ifndef NCOUT
			cout << " Infeasible (artificial)" << endl;
			#endif
			cuts++;			
			return;
		}
		IloNum lb = cgModel.cplex.getObjValue();		
		cout << " obj " << lb << " Status " << cg_status << " UB " << ub << endl;
		#ifndef NCOUT
		cout << " Solution obj " << lb << endl;
		double value = 0;
		for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
			if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
				int j, c, type;		
				cgModel.getColumnIndices(it.getVar(), j, c, type);				
				double costCjs = cgModel.cplex.getValue(it.getVar())*cgModel.cjS[j][c];
				double realCost = cgModel.cplex.getValue(it.getVar())*(inst.cJ[j]  +  IloScalProd(cgModel.delta[j][c], inst.cIJ[j]) );
				cout << it.getVar().getName() << "*coef : " << cgModel.cplex.getValue(it.getVar()) << "*";
				for (int i = 0; i < inst.wells; i++) cout << cgModel.delta[j][c][i] << ",";
				cout  << " = " << it.getCoef()*cgModel.cplex.getValue(it.getVar()) << " - Cost-cjS " << costCjs << " Real cost " <<  realCost << endl;
				//~ for (int i = 0; i < inst.wells; i++) cout << subproblem[j].x[i].getUB() << " ";				
			}		
		}				
		cout << endl << "UB ";
		for (int i = 0; i < inst.compressors; i++) cout << cgModel.FacilityAssignment[i].getUB() << " ";
		cout << endl << "LB ";
		for (int i = 0; i < inst.compressors; i++) cout << cgModel.FacilityAssignment[i].getLB() << " ";
		#endif
		//Integral
		if (cg_status == 2){ 			
			cuts++;
			#ifndef NCOUT
			cout << " Integral ";
			#endif
			//New best upper bound
			if(lb < ub){
				ub = lb;
				#ifndef NCOUT
				cout << " New UB! " ;
				#endif
				///Clear the previous result and Storage the result - TODO improve the form to get the result
				//~ for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				//~ for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					//~ if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						//~ int j, c, type;		
						//~ cgModel.getColumnIndices(it.getVar(), j, c, type);
						//~ solution[j] = cgModel.delta[j][c];
					//~ }
				//~ }
			}
			return;
		}				
		if (lb >= ub){
			#ifndef NCOUT
			cout << "worst than UB" << ub << endl;
			#endif
			cuts++;
			return;
		}
///Storage the relaxed RMP model variables values and names		
		cgModel.relaxModelValues = NumNumMatrix(env, inst.compressors);				
		for (int j = 0; j < inst.compressors; j++){
			cgModel.relaxModelValues[j] = IloNumArray(env, cgModel.lambda[j].getSize());
			cgModel.cplex.getValues(cgModel.lambda[j], cgModel.relaxModelValues[j]);
			sum_colJ[j] = IloSum(cgModel.relaxModelValues[j]);
		}
		
///Solve the RMP in integer form		
		#ifndef NRMPZ	
		IloArray<IloConversion> conv(env, inst.compressors);

//Convert the lambda variables to INT 
		for (int j = 0; j < inst.compressors; j++){			
			conv[j] = IloConversion(env, cgModel.lambda[j], ILOINT);
			cgModel.model.add(conv[j]);			
		}		
		timer_ms.start();
		if(cgModel.cplex.solve()){		
			mp_time += timer_ms.total();
			IloNum obj = cgModel.cplex.getObjValue();	
			if (obj < ub){
				ub = obj;
				//~ ///Clear the previous result and Storage the result
				//~ for (int j = 0; j < inst.compressors; j++) solution[j] = IloNumArray(env, inst.wells);
				//~ for (IloExpr::LinearIterator it = cgModel.cost.getLinearIterator(); it.ok(); ++it){
					//~ if(cgModel.cplex.getValue(it.getVar()) >= RC_UB){
						//~ int j, c, type;		
						//~ cgModel.getColumnIndices(it.getVar(), j, c, type);
						//~ solution[j] = cgModel.delta[j][c];												
					//~ }
				//~ }			
			}						
		}else{			
			mp_time += timer_ms.total();
		}
	
		for (int j = 0; j < inst.compressors; j++){ //Remove the conversion
			cgModel.model.remove(conv[j]);						
		}
		#endif
	}else{//Infeasible 
		#ifndef NCOUT
		cout << "RMP Infeasible" << endl;
		#endif
		cuts++;		
		return;
	}
	#ifndef NCOUT
	cout << endl << " Fractional " << endl;
	#endif

///Fractional soltion
//Identify the level type of the next node and find the y (1st) or lambda(2st) variable that is closer to integrality
	unsigned int  typeNode = 2; //Type of the next node 1 or 2
	IloNum varValue;
	int compressor = -1, well=-1;	
//1st level
	if(node.type == 1){
		for (unsigned int j = 0; j < inst.compressors; j ++){
			if (sum_colJ[j] > RC_UB && sum_colJ[j] < NEAR_ONE){
				typeNode = 1;
				cgModel.findCloseIntegralityLevel1(env, inst, varValue, compressor, sum_colJ);
				goto createNode;
			}
		}
	}
//2nd level	
	cout << "Type node " << typeNode << endl;	
	cgModel.findCloseIntegralityLevel2(env, inst, varValue, compressor, well);	
	
	createNode:
	IloNumArray b(env, 2); 
	//Define the order of the RHS //TODO pegar a mais fracionária
	if (varValue < 0.5){ //Default close to 0, 0 first
		b[0] = 0;
		b[1] = 1;
	}else{
		b[0] = 1;
		b[1] = 0;
	}
	for (int i = 0; i < b.getSize(); i++){
		#ifndef NCOUT
		cout << "Index " << i << " branch rule " << compressor << " : " << b[i] << endl;
		#endif
		///Create the node 
		nodeBP nodeB(env, compressor, well, b[i], typeNode);
		//Copy the UB of the variables x of the previous node - TODO: its need only for 2nd level
			//~ cout << "PASSOU " << endl;
			nodeB.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				nodeB.ubPricing[j] = IloIntArray(env, inst.wells);
				for (int id = 0; id < inst.wells; id++){
					nodeB.ubPricing[j][id] = node.ubPricing[j][id];

				}
			}
///Verify Node Type			
		if (nodeB.type == 1){
			if (b[i] >= NEAR_ONE){ 
				cgModel.FacilityAssignment[nodeB.j].setLB(1);
			}else{ //b[i] == 0
				cgModel.FacilityAssignment[nodeB.j].setUB(0);
			}
		}else{//Type = 2			
			//Insert the pricing constraints and storage the modifications in the x variables in this node.
			for (int j = 0 ; j < inst.compressors; j++){
				if(b[i] == 0){
					if(j == nodeB.j){
						cgModel.BranchConst[j][nodeB.i].setUB(0); //RMP
						subproblem[j].x[nodeB.i].setUB(0); 		//Pricing
						nodeB.ubPricing[j][nodeB.i] = 0;		
					}
				}else{ //b[i]==1
					if(j == nodeB.j){
						cgModel.BranchConst[j][nodeB.i].setLB(1); //RMP
						subproblem[j].x[nodeB.i].setLB(1); //Pricing								
					}else if(node.ubPricing[j][nodeB.i] == 1){ //The ub is set to 0 and stored in ubPricing only if in the previos x.ub was 1
						subproblem[j].x[nodeB.i].setUB(0); //Pricing
						nodeB.ubPricing[j][nodeB.i] = 0;   //    
					}
				}
			}

		}
		///Branch and price recursive
		Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, nodeB, ub, solution);		
		
		#ifndef NCOUT
		cout << "Removed: " << i << " branch rule " << compressor << " : " << b[i] << endl;
		#endif

		//Remove restrictions 
		if(nodeB.type == 1){
			if (b[i] >= NEAR_ONE){
				cgModel.FacilityAssignment[nodeB.j].setLB(0);
			}else{ //b[i]=0
				cgModel.FacilityAssignment[nodeB.j].setUB(1);			
			}
		}else{///Type == 2
			//Remove restrictions of the pricing in j
			if (b[i] == 0){
				cgModel.BranchConst[nodeB.j][nodeB.i].setUB(1);
				subproblem[nodeB.j].x[nodeB.i].setUB(1);
				nodeB.ubPricing[nodeB.j][nodeB.i] = 1;
			}else{//b[i]=1
				cgModel.BranchConst[nodeB.j][nodeB.i].setLB(0);
				subproblem[nodeB.j].x[nodeB.i].setLB(0);			
			}
			
			for (int comp = 0; comp < inst.compressors; comp++){
				//Remove restrictions of the pricing in N\j 
				if (b[i] == 1){
					if(comp != nodeB.j && node.ubPricing[comp][nodeB.i] >= NEAR_ONE){
						subproblem[comp].x[nodeB.i].setUB(1);
						nodeB.ubPricing[comp][nodeB.i] = 1;					
					}
				}
			}
		}
	}
}

void csp::bPrice(string file, const int n_ini, const int n_fim){
	stringstream out;
	//~ cout.precision(dbl::max_digits10); //Max precision in the COUT
	out << 4 << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}

	output << "I\t" << "N\t" << "M\t" << "RMP\t" << "Pricing\t" << "Total\t" << "Obj\t" <<  "Nodes\t" << "Cuts\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}		
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float mp_time {0};
		float p_time {0};	
		float totalTime {0};
		
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
				
		IloNum ub = {numeric_limits<double>::max()};			
		cuts = 0;
		nodes = 0;		
		NumNumMatrix solution(env,inst.compressors);
		for (int j = 0; j < inst.compressors; j++){
			solution[j] = IloNumArray(env, inst.wells);
		}
		
		try{
			//Initialize Master 
			CGModel cgModel(env);			
			cgModel.init(env, inst.compressors, inst.wells);
			
			//Initialize Pricings 
			IloArray<pricing> subproblem(env, inst.compressors);
			for (int j = 0; j < inst.compressors; j++){
				subproblem[j] = pricing(env);
				subproblem[j].init(env, inst, j);			
			}
			//Generate initial columns
			cgModel.greedyIntialColumns(env, inst);				

			nodeBP node(env,0,0,0,1);
			//~ node.restrictedPricing = IloArray<int>(env,inst.compressors);
			node.restrictWell = IloIntArray(env, inst.wells);
			node.ubPricing = IloArray<IloIntArray>(env, inst.compressors);
			for (int i = 0; i < inst.compressors; i++){
				node.ubPricing[i] = IloIntArray(env, inst.wells);
				for (int j = 0; j < inst.wells; j++){
					node.ubPricing[i][j] = 1;
				}
			 }
			//BEGIN OF RECURSION			
			Branch_Price(env, inst, cgModel, subproblem, timer_ms, mp_time, p_time, node, ub, solution);
			totalTime += timer_global.total();
			
			output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << mp_time/1000 << "\t" << p_time/1000 << "\t" << totalTime/1000 << "\t" << ub << "\t" << nodes << "\t" << cuts << "\t";
			//~ double columnCost = 0;
			//~ for (int j = 0; j < inst.compressors; j++){
				//~ output << j+1 << "{";
				//~ int ativo = 0;
				//~ for (int i = 0; i < inst.wells; i++){					
					//~ if (solution[j][i] >= NEAR_ONE){
						 //~ output << i+1 << ",";
						 //~ columnCost += inst.cIJ[j][i];//*solution[j][i];
						 //output << "[" << inst.cIJ[j][i] << "]";
						 //~ ativo++;
					//~ }
				//~ }
				//~ if (ativo > 0){
					 //~ columnCost += inst.cJ[j];
					 //output << "[" << inst.cJ[j] << "]";
				//~ }	
				//~ output << "}";
			//~ }
			//~ 
			//~ output << " - COST: " << columnCost << endl;
			
		}catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
		}
		catch (...) {
		  cerr << "Unknown exception caught" << endl;
		}		  
	   env.end();	
	}		
   output.close();   
}
