#include "../include/cg.h"
#include "../include/bprice.h"

using namespace std;

int main(const int argc, char** argv) {
 if (argc != 5) {
	cout << "Error! Choose the correct params" << endl;
	cout << "<Model> <Intance prefix> <initial number> <final number>" << endl;
	cout << "Model: 1 = MILP relaxation; 2 = MILP,  3 = Column Generation; 4 = Branch and Price" << endl;
 }
 
 int modelId = atoi(argv[1]);
 stringstream file;
 file << argv[2];
 int n_ini = atoi(argv[3]);
 int n_fim = atoi(argv[4]);
 switch (modelId){
	case 1: csp::milp(file.str(), n_ini, n_fim, 1);
	break;
	case 2: csp::milp(file.str(), n_ini, n_fim, 2);
	break;
	case 3: csp::cg(file.str(), n_ini, n_fim);
	break;
	case 4: csp::bPrice(file.str(), n_ini, n_fim);
	break;		
	default: cout << "Error, no Model with index " << modelId << " was not found! Try again!"<< endl;
	break;
 }
 
 
return 0;
}
