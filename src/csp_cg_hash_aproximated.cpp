// Date: November/2014
// Description: Column Generation for Compressor Scheduling Problem
// Based on: "Column Generation for Solvin a Compressor Scheduling Problem" and "A revised Model for compressor designand scheduling in gas-lifted oil fields
// Eduardo Camponogara and Augustinho Plucenio
// Version optimized with new index for the matrices
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"

#define RC_LB 0
//~ -1.0e-6

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;

void Instance::readFile(IloEnv& env, ifstream& input){
	cJ = IloNumArray(env); 
	dJ = IloNumArray(env); 
	qCMin = IloNumArray(env); 
	qCMax = IloNumArray(env); 
	alpha0 = IloNumArray(env); 
	alpha1 = IloNumArray(env); 
	alpha2 = IloNumArray(env); 
	alpha3 = IloNumArray(env); 
	alpha4 = IloNumArray(env); 
	pointsQ = NumNumMatrix(env); 
	pointsH = NumNumMatrix(env);

	qW = IloNumArray(env); 
	pW = IloNumArray(env); 

	orac = NumNumMatrix(env); 
	cIJ = NumNumMatrix(env); 
	lIJ = NumNumMatrix(env);  
	qMaxIJ = NumNumMatrix(env); 

	string s;
	IloNum c, d;
	IloInt temp, x;
	
	//Parameters
	input >> compressors >> wells >> k;	
	
	/*****Scan file*****/
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}	
	//Compressor data	
	for (int i = 0; i < compressors; i++){
		pointsQ.add(IloNumArray(env, k));
		pointsH.add(IloNumArray(env, k));		
		
		cIJ.add(IloNumArray(env, wells));		
		lIJ.add(IloNumArray(env, wells));		
		qMaxIJ.add(IloNumArray(env, wells));
		orac.add(IloNumArray(env, wells));	
		
		double compCost, energyCost, rateMin, rateMax, a0, a1, a2, a3, a4;
		input >> temp >> compCost >> energyCost >> rateMin >> rateMax >> a0 >> a1 >> a2 >> a3 >> a4;
		cJ.add(compCost);
		dJ.add(energyCost);
		qCMin.add(rateMin);
		qCMax.add(rateMax);
		alpha0.add(a0);
		alpha1.add(a1);
		alpha2.add(a2);
		alpha3.add(a3);
		alpha4.add(a4);
		
	}
	//Well data
	for (int i = 0; i < 3; i++){
		getline(input, s);
	}
	for (int i = 0; i < wells; i++){		
		double gasCli, pCli;
		input >> temp >> gasCli >> pCli; //percorre até pw
		qW.add(gasCli);
		pW.add(pCli);
		
		getline(input, s); //pega o Ni
		stringstream ss(s); //Define a string como stream
		while(ss >> c){   //Take the doubles
			orac[c-1][i] = 1;						
		}
	}
	//Pipeline
	for (int i = 0; i < 2; i++){
		getline(input, s);
	}	
	while(getline(input, s)){
		if(s == "Service cost")break;
		stringstream(s) >> temp >> x >> c;
		lIJ[x-1][temp-1] = c;		
	}
	
	//Service cost
	getline(input, s);
	while(getline(input, s)){
		if(s == "Points")break;
		stringstream(s) >> temp >> x >> c;
		cIJ[x-1][temp-1] = c; 				
	}
	//Points
	getline(input, s);
	while(getline(input, s)){
		if(s == "Max output gas-rate")break;
		double p;
		stringstream(s) >> temp >> x >> c >> p;
		pointsQ[temp-1][x] = c; 
		pointsH[temp-1][x] = dJ[temp-1]*c*p;				
	}	
	//Max output gas-rate
	getline(input, s);
	while(getline(input, s)){
		stringstream(s) >> temp >> x >> c;
		qMaxIJ[x-1][temp-1] = c;		
	}	
}

double getPressure(const Instance& inst, const int& j, const double& qj){
	long double gasRate;
	if (qj == 0){
		gasRate = inst.qCMin[j];		
	}else {
		gasRate = qj;
	}
	return inst.alpha0[j] + inst.alpha1[j]*gasRate + inst.alpha2[j]*(pow(gasRate,2.0)) + 
	inst.alpha3[j]*(pow(gasRate,3.0)) + inst.alpha4[j]*(log(1 + gasRate));	
}

double costColum(const IloInt& wells, const IloNumArray& delta, const IloInt& compressor, const IloNumArray& cJ, 
				 const NumNumMatrix& cIJ, const IloNumArray& dJ, const IloNum& qC, const IloNum& pC){
	  IloNum cost = cJ[compressor];
	  cost += IloScalProd(cIJ[compressor], delta);
	  cost += dJ[compressor]*qC*pC;
	  return cost;
}
  		
void CGModel::init(IloEnv& env, const int& compressors, const int& wells){
    env.setNormalizer(IloFalse);
	//cplex.setParam(IloCplex::NumericalEmphasis, 1);    //Precisão numérica
	//cplex.setParam(IloCplex::RootAlg, IloCplex::Dual);
	//cplex.setParam(IloCplex::AdvInd, 0); //Não utilizar informação anterior na execução 
			
    lambda = NumVarMatrix(env,compressors); //Array de Array de variáveis [n]x[#col]
    delta = IloArray<NumNumMatrix>(env, compressors);
    qjS = NumNumMatrix(env, compressors);
    cjS = NumNumMatrix(env, compressors);
    
    ClientServiced = IloRangeArray(env, wells, 1, 1);
    FacilityAssignment = IloRangeArray(env, compressors, -IloInfinity, 1);
    //~ cplex.setOut(env.getNullStream());
}

void pricing::init(IloEnv& env, const Instance& inst, const IloInt& j){
	//cplex.setParam(IloCplex::AdvInd, 0);
	//~ cplex.setOut(env.getNullStream());
	int i, ka;
	int n = inst.compressors;
	int m = inst.wells;
	int k = inst.k;
	//Variables
	x = IloNumVarArray(env, m, 0, 1, ILOINT);
	y = IloNumVarArray(env, m, 0, 1, ILOINT); //Extra
	q = IloNumVar(env);//, 0, IloInfinity,ILOFLOAT);
	z = IloNumVarArray(env, k, 0, 1, ILOINT);
	lambdaL = IloNumVarArray(env, k, 0, IloInfinity, ILOFLOAT);
	lambdaR = IloNumVarArray(env, k, 0, IloInfinity, ILOFLOAT);
	//~ x.setNames("x");
	y.setNames("y");
	q.setName("q");
	z.setNames("z");
	lambdaL.setNames("lL");
	lambdaR.setNames("lR");
	
	//Objective
	IloExpr expr(env);
	expr.clear();
	for (i = 0; i < m; i++){
		if(inst.orac[j][i] == 1) expr += x[i]; 
	}
	for (ka = 1; ka < k; ka++){
		expr += (inst.pointsH[j][ka-1]*lambdaL[ka] + inst.pointsH[j][ka]*lambdaR[ka]);
	}
	SPj.setExpr(expr);
	model.add(SPj);
	
	//Constraints
	//3b
	expr.clear();
	for (ka = 1; ka < k; ka++){
		expr += (inst.pointsQ[j][ka-1]*lambdaL[ka] + inst.pointsQ[j][ka]*lambdaR[ka]);
	}
	model.add(q == expr);
	
	//3c - modified with new variable y1 (20-02-15)
	for(i = 0; i < m; i++){
		if (inst.orac[j][i] == 1){ 
			model.add(q <= inst.qMaxIJ[j][i]*x[i] + inst.qCMax[j]*y[i]);		
			model.add(x[i]+y[i] == 1);
		}
		//~ if (inst.orac[j][i] == 1) model.add(q <= inst.qMaxIJ[j][i]*x[i] + inst.qCMax[j]*(1.0 - x[i]));				
	}

	//3d
	expr.clear();	
	//~ for(i = 0; i < m; i++){
		//~ if (inst.orac[j][i] == 1) expr+= inst.qW[i]*x[i];
	//~ }
	expr = IloScalProd(inst.qW,x);
	model.add(expr <= q);
	//~ model.add(expr == q);  //Original constraint of the problem in case 2008
	
	//3e
	model.add(IloSum(z) == 1);
	
	//3f
	for (ka = 1; ka < k; ka++){
		model.add(lambdaL[ka]+lambdaR[ka] == z[ka]);
	}
	
	for (i = 0; i < m; i++){
		//3i & extra - restringir a 0 as variaveis x que não fazem parte de MJ
		if (inst.orac[j][i] == 0) model.add(x[i] == IloFalse);		
	}
}
void pricing::setDualCoef(const IloInt& j, const IloNum& miD, const IloNumArray& piD, const Instance& inst){
	//~ SPj.setLinearCoefs(x, inst.cIJ[j]-piD);
	for (int i = 0; i < inst.wells ; i++){
		if (inst.orac[j][i] == 1){
			SPj.setLinearCoef(x[i], inst.cIJ[j][i]-piD[i]);
		}
		else{
			SPj.setLinearCoef(x[i], 0);
		}
	}
	SPj.setConstant(inst.cJ[j] - miD);
}
void pricing::run(){
	cplex.solve();
}
void CGModel::greedyIntialColumns(IloEnv& env, const Instance& inst, unordered_map<string,int>& pattern){
	int i, j, c;
	for(j = 0; j < inst.compressors; j++){
		delta[j] = NumNumMatrix(env);
		delta[j].add(IloNumArray(env, inst.wells));
				
		qjS[j] = IloNumArray(env);
		qjS[j].add(0);
		
		lambda[j] = IloNumVarArray(env);
		IloNum qmin = 100.0;
		c = delta[j].getSize()-1;
		string p = to_string(j)+":";
		for (i = 0; i < inst.wells; i++){			
			if (inst.orac[j][i] == 1 && (qjS[j][c]+inst.qW[i]) <= inst.qMaxIJ[j][i] && (qjS[j][c]+inst.qW[i]) <= qmin){
				p+= to_string(1);
				delta[j][c][i] = 1;
				qjS[j][c] += inst.qW[i];
				if (inst.qMaxIJ[j][i] < qmin){
					qmin = inst.qMaxIJ[j][i];
				}				
			}else if (inst.orac[j][i] == 1 && (qjS[j][c]+inst.qW[i]) > qmin){
				delta[j].add(IloNumArray(env, inst.wells));
				qjS[j].add(inst.qW[i]);
				
				c = delta[j].getSize()-1;
				delta[j][c][i] = 1;				
				qmin = inst.qW[i];				
			}
		}
		//Slack variables (to ensure initial feasible solution for RMP)
		delta[j].add(IloNumArray(env, inst.wells));
		c = delta[j].getSize()-1;
		for(i = 0; i < inst.wells; i++){
			delta[j][c][i] = 1;
		}
		
		//Add var to the model
		for (int col = 0; col < c; col++){			
			//Greedy
			lambda[j].add(cost(costColum(inst.wells, delta[j][col], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][col], 
							getPressure(inst, j, qjS[j][col])) ) );
			lambda[j][col].setName("l");			
		}		
		//Slack
		lambda[j].add(cost(pow(IloSum(inst.cJ),2)));
		lambda[j][c].setName("s");		
			
	}
	addConstraints(env);
}
void CGModel::genInitRMP(IloEnv& env, const Instance& inst, unordered_map<string,int>& pattern){
	 int j,i,c;
	 string p;
	 ///*Pré-Init*///
	 for(j = 0; j < inst.compressors; j++){
		//lambda.add(IloNumVarArray(env));
		delta[j] = NumNumMatrix(env);
		qjS[j] = IloNumArray(env);
		cjS[j] = IloNumArray(env);
		for(c = 0; c < inst.wells; c++){ 
			if(inst.orac[c][j] == 1){ // #col = |Mj|
				qjS[j].add(0);
				cjS[j].add(0);			
				delta[j].add(IloNumArray(env));
			}
		}
		lambda[j] = IloNumVarArray(env);
		for(int x = 0; x < delta[j].getSize(); x++){
			for(i = 0; i < inst.wells; i++){
				delta[j][x].add(0);
			}
		}
	 }
	 ///*End Pré-Init*///
		 for(j=0; j < inst.compressors; j++){ 
			int lastId = -1;
			for (c=0; c < delta[j].getSize(); c++){
				for (i=0; i < inst.wells; i++){ 
					if(inst.orac[i][j] == 1){ 
						if (i > lastId){ 
							//Assumindo que todo compressor possui gas-rate superior a qualquer poço, não é necessário o if abaixo 
							//TODO: caso essa afirmação não seja verdadeira, não pode gerar algumas colunas
							//if(inst.qW[i] <= inst.qCMax[j]){ 
								delta[j][c][i] = 1;
								//Verificar max{qWi,qcMin}
								if(inst.qW[i] > inst.qCMin[j]) qjS[j][c] = inst.qW[i];
								else qjS[j][c] = inst.qCMin[j];
									
								//Verificar pressão: se viola a restrição, tira o cliente e a coluna fica vazia, mas o compressor ativo
								if(inst.qMaxIJ[i][j] < qjS[j][c]){
									delta[j][c][i] = 0;
									qjS[j][c] = inst.qCMin[j];
								}
							//}
							cjS[j][c] = costColum(inst.wells, delta[j][c], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][c], 
							getPressure(inst, j, qjS[j][c]));
							
							lambda[j].add(cost(cjS[j][c]));
							lambda[j][lambda[j].getSize()-1].setName("l");
							
							lastId = i;
							break;
						}
					} 
				}
				p = to_string(j)+":";
				for(i = 0; i < inst.wells; i++){
					p+= to_string((int)delta[j][c][i]);
				}
				pattern.insert({p,1});
				
			}
			//Slack variables (to ensure initial feasible solution for RMP)
			delta[j].add(IloNumArray(env));
			p = to_string(j)+":";
			for(i = 0; i < inst.wells; i++){
				delta[j][delta[j].getSize()-1].add(1);
				p+=to_string(1);				
			}
			pattern.insert({p,1});
						
			if (inst.wells <= inst.compressors) lambda[j].add(cost(pow(IloSum(inst.cJ),2)));
			else lambda[j].add(cost(pow(IloSum(inst.cJ),2)));
			
			lambda[j][lambda[j].getSize()-1].setName("s");
		}		
		addConstraints(env);
}
void CGModel::genInitColumnManual(IloEnv& env, const Instance& inst, unordered_map<string,int>& pattern){
 int j,i,c;
	  ///*Pré-Init*///
	 for(j = 0; j < inst.compressors; j++){
		delta[j] = NumNumMatrix(env);
		qjS[j] = IloNumArray(env);
		cjS[j] = IloNumArray(env);
		for(c = 0; c < 2; c++){ 
			qjS[j].add(0);
			cjS[j].add(0);			
			delta[j].add(IloNumArray(env, inst.wells));
		}
		lambda[j] = IloNumVarArray(env);
	 }
	 ///*End Pré-Init*///
	 delta[0][0][0]=1;
	 delta[0][0][1]=1;
	 delta[0][0][2]=1;
	 
	 delta[1][0][3]=1;
	 delta[1][0][5]=1;
	 
	 delta[2][0][4]=1;
	 
	 delta[3][0][3]=1;
	 
	 delta[4][0][4]=1;
	 
	 delta[0][1][1]=1;
	 delta[0][1][2]=1;
	 
	 delta[1][1][0]=1;
	 delta[1][1][5]=1;
	 
	 delta[2][1][3]=1;
	 
	 delta[3][1][5]=1;
	 
	 delta[4][1][5]=1;
	 for (j = 0; j < inst.compressors; j++){
		for (c = 0; c < delta[j].getSize(); c++){
			string p = to_string(j)+":";
			for (i = 0; i < inst.wells; i++){
				qjS[j][c] += inst.qW[i]*delta[j][c][i];
				p+= to_string((int)delta[j][c][i]);
			}
			pattern.insert({p,1});
			cjS[j][c] = costColum(inst.wells, delta[j][c], j, inst.cJ, inst.cIJ, inst.dJ, qjS[j][c], 
			getPressure(inst, j, qjS[j][c]));
			
			lambda[j].add(cost(cjS[j][c]));
		}
	 }
	 addConstraints(env);
 }

void CGModel::addConstraints(IloEnv& env){
	//5.b
	  for(int i = 0; i < ClientServiced.getSize(); i++){
		  IloExpr rest(env);
		  for (int j = 0; j < FacilityAssignment.getSize(); j++){
			for(int c = 0; c < lambda[j].getSize(); c++){
				rest += delta[j][c][i]*lambda[j][c];
			}	
		  }
		  ClientServiced[i].setExpr(rest);
		  rest.end();
	  }
	  model.add(ClientServiced);
	  // 5.c
	  for (int j = 0; j < FacilityAssignment.getSize(); j++){
		 FacilityAssignment[j].setExpr(IloSum(lambda[j]));
	  }
	  model.add(FacilityAssignment);
}	

void CGModel::addColumn(const IloInt& j, const IloNum& columnCost, const IloNumArray& coef, const Instance& inst){
	//Objective and constraint 5b
	lambda[j].add(cost(columnCost) + ClientServiced(coef));
	//Name
	lambda[j][lambda[j].getSize()-1].setName("l");
	//5c
	FacilityAssignment[j].setExpr(IloSum(lambda[j]));
}

bool CGModel::isFeasible(const IloNum& n){
	int j, c;
	for(j = 0; j < n; j++){
		for(c = 0; c < lambda[j].getSize(); c++){
			if (strcmp(lambda[j][c].getName(), "s") == 0){
				if(cplex.getValue(lambda[j][c]) > 0.0){
					return false;
				}
			}
		}
	}
	return true;
}

void CGModel::run(){
	cplex.solve();	
}

bool columnExists(const unordered_map<string,int>& pattern, const int& j, const IloNumArray& x, string& new_pattern){
	new_pattern = to_string(j)+":";
	for (int i = 0; i < x.getSize(); i++){
		new_pattern += to_string((int)x[i]);
	}
	if(pattern.count(new_pattern) > 0){
		return true;
	}
	return false;
}

int main(int argc, char **argv) {
	////////Time parameters
	Timer<chrono::milliseconds> timer_ms;
	Timer<chrono::milliseconds> timer_global;
	timer_global.start();
	float mp_time {0};
	float p_time {0};
		
	ifstream inFile;
	if(argc > 1)
		inFile.open(argv[1]);
	else
		cerr << "Miss argument: <instance name> " << endl;
	
	if (inFile.fail()) {
      cerr << "Unable to open instance"<< endl;
      exit(1);
	}
	
	IloEnv   env; 
	Instance inst(env);
	inst.readFile(env, inFile);
	unordered_map<string,int> pattern; 
	try{
		CGModel cgModel(env);
		cgModel.init(env, inst.compressors, inst.wells);
		//Generate initial columns
		//~ cgModel.genInitRMP(env, inst, pattern);
		//~ cgModel.genInitColumnManual(env, inst, pattern);
		cgModel.greedyIntialColumns(env, inst, pattern);
				
		//Print model
		//~ stringstream filename;
		//~ filename << "cg.lp";
		//~ cgModel.cplex.exportModel(filename.str().c_str());
		
		timer_ms.start();
		cgModel.run();		
		mp_time += timer_ms.total();
		
		int cont; //Controla o nº de colunas adicionadas ao master
		IloNumArray reducedCost(env, inst.compressors);
		NumNumMatrix coef(env, inst.compressors); 
		
		int i,j, ka;
		int n = inst.compressors;
		int m = inst.wells;
		int k = inst.k;
		
		IloArray<pricing> subproblem(env, inst.compressors);
		for (j = 0; j < inst.compressors; j++){
			subproblem[j] = pricing(env);
			subproblem[j].init(env, inst, j);
			coef[j] = IloNumArray(env, inst.wells);
		}
				
		IloNumArray pi(env, inst.wells);
		IloNumArray mi(env, inst.compressors);	
		string new_pattern;
		for(;;){
			//Get dual variables
			cgModel.cplex.getDuals(pi, cgModel.ClientServiced); //5b
			cgModel.cplex.getDuals(mi, cgModel.FacilityAssignment); //5c
			
			cont = n; 
			for(j = 0; j < n; j++){
				subproblem[j].setDualCoef(j, mi[j], pi, inst);
				//Execute pricing
				timer_ms.start();
				
				//Print model
				//~ stringstream filename;
				//~ filename << "pricing.lp";
				//~ subproblem[j].cplex.exportModel(filename.str().c_str());
		
				subproblem[j].run();
				p_time +=  timer_ms.total();
				//Storage objectiveValue and varX
				reducedCost[j] = subproblem[j].cplex.getObjValue();
				subproblem[j].cplex.getValues(coef[j], subproblem[j].x);
				double columnCost = reducedCost[j] + mi[j] + IloScalProd(coef[j], pi);
				while(reducedCost[j] < RC_LB){
					new_pattern="";
					/// Rotina para verificar se a coluna gerada não é igual a alguma já existente
					if (columnExists(pattern, j, coef[j], new_pattern)){
						//addConstrant ao pricing j
						IloExpr rest(env);
						for (i = 0; i < m; i++){
							if ((int)coef[j][i] == 1){
								rest += ((int)coef[j][i]-subproblem[j].x[i]);
							}else{
								rest += subproblem[j].x[i];
							}
						}
						subproblem[j].model.add(rest >= 1);
						rest.end();
						timer_ms.start();
						subproblem[j].run();
						p_time +=  timer_ms.total();
						subproblem[j].cplex.getValues(coef[j], subproblem[j].x);
						reducedCost[j] = subproblem[j].cplex.getObjValue();
						columnCost = reducedCost[j] + mi[j] + IloScalProd(coef[j], pi);
					///*******************************************
					}else{
						//~ cgModel.addColumn(j, subproblem[j].cplex.getValue(subproblem[j].q), coef[j], inst); //Coluna do Pricing
						cgModel.addColumn(j, columnCost, coef[j], inst); //Coluna do Pricing
						pattern.insert({new_pattern,1});
						cont--;
						break;
					}					
				}
			}
			if (cont == n){ //Caso nenhuma coluna seja adicionada, i.e. todos SPj são >=0
				
				float totalTime = timer_global.total();
				cout << "Total Time: " << totalTime << endl;
				cout << "Time spend to Master Problem: " << mp_time << " (" << mp_time/totalTime*100 << "%)" << endl;				
				cout << "Time spend to Pricing " << p_time << " (" << p_time/totalTime*100 << "%)" <<endl;				
				
				printf("FIM DO CG --------------- Objective value....: %f\n", cgModel.cplex.getObjValue());
				IloNumArray values(env);
				for (int j = 0; j < inst.compressors; j++){
					cgModel.cplex.getValues(values, cgModel.lambda[j]); 
					cout << "Solution - Compressor " << j+1 << " : " << values << endl ;
			    }			   
				
				if (cgModel.isFeasible(inst.compressors)) cout << "Final Solution is Feasible" << endl;
				else cout << "Final Solution is Infeasible" << endl;
				
				break;
				
			}else{
				timer_ms.start();
				cgModel.run();
				mp_time +=  timer_ms.total();				
			}
		}
		
		
		
	}
	catch (IloException& e) {
      cerr << "Concert exception caught: " << e << endl;
	}
	catch (...) {
      cerr << "Unknown exception caught" << endl;
	}
      
   env.end();
   return 0;
}
