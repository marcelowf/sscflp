// -------------------------------------------------------------- -*- C++ -*-
// Autor: Marcelo Friske
// Date: September/2014
// Description: Algorithm for Compressor Scheduling Problem, picewise linear formulation
// with binary formulations. Based on: "A revised model for compressor design and scheduling
// in gas-lifted oil fields". E. Camponogara, L. Nazari and C. Meneses (2011).
// PS: Com adição de restrições para definir os subconjuntos Ni e Mj
// 
// --------------------------------------------------------------------------

#include "../include/cg.h"
#include "../include/util.h"

#define NEAR_ONE 0.999999

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<IloNumArray> NumNumMatrix;

using namespace std;
using csp::Instance;

	
void csp::milp(string file, const int n_ini, const int n_fim, const int type){	
	stringstream out;
	out << type << "_Inst_" << n_ini << "_to_" << n_fim << ".txt";
	ofstream output(out.str());		
	if (output.fail()){
		cerr << "Unable to create output file" << endl;
	}
	output << "I\t" << "N\t" << "M\t" << "Cplex\t" << "Total\t" << "Obj\t" << "Solution" << endl;
	
	for (int ins = n_ini ; ins <= n_fim; ins++){
		ifstream inFile;
		stringstream in;
		in << file << ins << ".txt";		
		inFile.open(in.str());
		
		if (inFile.fail()) {
			cerr << "Unable to open instance"<< endl;
			exit(1);
		}
		////////Time parameters
		Timer<chrono::milliseconds> timer_ms;
		Timer<chrono::milliseconds> timer_global;
		timer_global.start();
		float cplex_time {0};
	
		IloEnv   env; 
		Instance inst(env);
		inst.readFile(env, inFile);
		
	   try {
		  IloModel model(env); 
		  int i, j;
		  int n = inst.compressors;
		  int m = inst.wells;
		  int k = inst.k;
		  
		  /**** Variables ****/
		  IloNumVarArray y(env, n, 0, 1, ILOINT); // yi -> facilities {0,1}		  
		  NumVarMatrix x(env, n); 				  // xij -> facility i assign to cliente j {0,1}
		  if (type==1){
			  for (i = 0; i < n; i++){
				x[i] = IloNumVarArray (env, m, 0, 1, ILOFLOAT);							
			  }
		  }else {
			  for (i = 0; i < n; i++){
				x[i] = IloNumVarArray (env, m, 0, 1, ILOINT);							
			  }
		  }		  
		  /***/
		  
		  IloExpr expr(env); 
		  /** Objective function ***/
		  expr.clear();
		  for(j = 0; j < n; j++){
			  expr += inst.cJ[j]*y[j]; 
		  }
		  for (j = 0; j < n; j++){
			for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)
				expr += inst.cIJ[j][i]*x[j][i]; 
			}
		  }		  
		  model.add(IloMinimize(env, expr));
		  
		  /***Constraints***/
		  //1b
		  for (i = 0; i < m; i++){
			for (j = 0; j < n; j++){
				if(inst.orac[j][i] == 1) 
				model.add(x[j][i] <= y[j]);
			}		
		  }
		  //1c
		  for(i = 0; i < m; i++){
			expr.clear();
			for (j = 0; j < n ; j++){
				if(inst.orac[j][i] == 1) 
				expr += x[j][i];
			}
			model.add(expr == 1); 
		  }
		  //1f
		  for(int j = 0; j < n; j++){
			  expr.clear();
			  for (i = 0; i < m; i++){
				if(inst.orac[j][i] == 1)	
				expr += inst.qW[i]*x[j][i];
			  }
			 model.add(expr <= inst.qCMax[j]); 		
		  }
	

		IloCplex cplex(model);
		//~ cplex.setOut(env.getNullStream());
		//~ cplex.setParam(IloCplex::Cliques, -1);
		//~ cplex.setParam(IloCplex::Covers, -1);
		//~ cplex.setParam(IloCplex::DisjCuts, -1);
		//~ cplex.setParam(IloCplex::FlowCovers, -1);
		//~ cplex.setParam(IloCplex::FlowPaths, -1);
		//~ cplex.setParam(IloCplex::FracCuts, -1);
		//~ cplex.setParam(IloCplex::GUBCovers, -1);
		//~ cplex.setParam(IloCplex::ImplBd, -1);
		//~ cplex.setParam(IloCplex::MIRCuts, -1);
		//~ cplex.setParam(IloCplex::ZeroHalfCuts, -1);
		timer_ms.start();
		cplex.solve();
		cplex_time += timer_ms.total();	  
		  
		float totalTime = timer_global.total();
		//~ output << inst.compressors << "-" << inst.wells << endl;
		//~ output << "Total Time: " << totalTime << endl;
		//~ output << "Time spend to MILP: " << cplex_time << " (" << cplex_time/totalTime*100 << "%)" << endl;		
		//~ output << "FO: " << cplex.getObjValue() << endl;
		output << ins << "\t" << inst.compressors << "\t" << inst.wells << "\t" << cplex_time/1000 << "\t" << totalTime/1000 << "\t" << cplex.getObjValue() << "\t" ;
		
		IloNumArray vals(env);
		cplex.getValues(vals, y);
		//~ cplex.out() << "Solution Y is .......: " << vals << endl <<endl;
		//~ cplex.out() << "Matrix .......: " << endl;
		double value = 0;		
		for (int i = 0; i < n; i++){
			if (vals[i] > NEAR_ONE){
				//~ output << "(" << inst.cJ[i] << ")"; 
				value += inst.cJ[i];
			}
			output << i+1 << "{";
			for (int j = 0; j < m; j++){
				if (inst.orac[i][j] == 1 && cplex.getValue(x[i][j]) >= NEAR_ONE){
					value += inst.cIJ[i][j];					
					output << j+1 << ",";
				 }
			}
			output << "} ";
		}
		output << "Value: " << value << endl;
	
	   }
	   catch (IloException& e) {
		  cerr << "Concert exception caught: " << e << endl;
	   }
	   catch (...) {
		  cerr << "Unknown exception caught" << endl;
	   }
		env.end();	   	
		
		
   }
}
